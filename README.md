DATALAKES
=========

This repository configuration files and scripts for the hydrodynamic model of Lake Geneva based on the [MITgcm](http://mitgcm.org) package, with support for multiple resolutions (currently 100m, 200m, 450m and 1km are explicitly implemented). Additional scripts can make use of additional data such as MeteoSwiss weather, river flows and water level balance measurements.

The implementation assumes that either we want to run the code in standalone fashion, or launch multiple parallel simulations using the [SPUX](http://spux.readthedocs.org) Bayesian inference package. For optimal integration into SPUX, the top-level execution script is written in python.

## Minimum requirements

* A version of the MITgcm package designed specifically for this model (available [here](https://github.com/safinenko/MITgcm)), with `MITGCM_ROOTDIR` pointing to it's root directory
* MPI compilers and Fortran
* The `netcdf` Fortran package
* Python 3.6 or later
* Python packages: `netCDF4`, `pandas`, `scipy`, `numpy` and `dataclasses`.

## Setup

* Add the contents of the root directory to `PYTHONPATH`
* (To be finished)
