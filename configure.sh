#!/bin/bash

# Shell script to handle the compilation and some of the organizational
# aspects of the MITgcm code. This code assumes that certain parameters in
# 'PythonScripts/config_settings.py' have been set accordingly. A run-time
# argument is required to determine the action to be taken:
#   'bathymetry'    - generate bathymetry
#   'datafiles'     - sets up run-time files (data*)
#   'clean'         - clean up the build, binary data and run-time folders
#   'clean_runtime' - remove files generated during run-time
#   'compile'       - sets up the compilation environment and compiles the code
#   'recompile'     - generates the MITgcm executable
#   'all'           - calls 'clean', bathymetry', 'datafiles', 'compile'
#
# Note: add
#
# cmds="clean clean_runtime bathymetry datafiles compile recompile all"
# complete -W "$cmds" "./configure.sh"
#
# to allow autocompletion for the configuration script

set -e

printred() { echo -e '\e[31m'"$1"'\e[39m'; }
printgreen() { echo -e '\e[32m'"$1"'\e[39m'; }

# Extract parameters from config_settings.py
config_file='PythonScripts/config_settings.py'
MPI_OPTION=$(grep withMPI "$config_file" | awk -F '=' '{print$2}' | xargs)
MESH_SIZE=$(grep meshSize "$config_file" | awk -F '=' '{print$2}' | xargs)

# Make sure that the extracted arguments make some sense
if [ "$MPI_OPTION" != "True" ] && [ "$MPI_OPTION" != "False" ]; then
    printred '✗ ERROR: could not parse the MPI option correctly' && exit 1
elif ! [[ "$MESH_SIZE" =~ ^[0-9]+$ ]]; then
    printred '✗ ERROR: could not parse mesh size (must be int)' && exit 1
fi


# Define functions to handle particular sub-tasks.
clean()
{
    # First remove the runtime files
    clean_runtime

    # Delete all the files located in '.gitignore'. This will delete the
    # mitgcm executable, compilation and input files.
    while read -r DIRECTORY
    do
        [ -e "$DIRECTORY" ] && rm -r "$DIRECTORY"
    done < .gitignore
    return 0
}


clean_runtime()
{
    # Clean files generated at runtime by MITgcm.
    if [ -d Hydrodynamics/run ]; then
        mkdir -p Hydrodynamics/temp
        (
            cd Hydrodynamics/run/
            for f in data* eedata mitgcmuv pickup.0000000000.*.nc; do
                if [ -e $f ] ; then
                    mv $f ../temp/
                fi
            done
            cd ..
            rm -r run
            mkdir run
            mv temp/* run
        )
        rmdir Hydrodynamics/temp
    fi
}


generate_bathymetry()
{
    # First, checks if a suitable bathymetry file has been generated. If not, we
    # will generate a new file.
    mkdir -p Hydrodynamics/binary_data
    if [ -f PythonScripts/template_bathymetry/bathyGeneva_"$MESH_SIZE"m.bin ]; then
        printgreen "✓ Bathymetry for mesh size ${MESH_SIZE} found."
        cp PythonScripts/template_bathymetry/bathyGeneva_"$MESH_SIZE"m.bin \
           Hydrodynamics/binary_data
    else
        mkdir -p PythonScripts/Output
        python3 PythonScripts/generate_bathymetry.py
        mv PythonScripts/Output/bathyGeneva_"$MESH_SIZE"m.bin \
        Hydrodynamics/binary_data
        echo "Bathymetry generated for mesh size ${MESH_SIZE}."
    fi
}


generate_datafiles()
{
    mkdir -p Hydrodynamics/run
    cp Hydrodynamics/template/data.obcs Hydrodynamics/run/data.obcs
    python3 PythonScripts/write_config_files.py
}


recompile()
{ (
    cd Hydrodynamics/build
    rm -f mitgcmuv
    make -j5 && printgreen 'INFO: Make exited without error.'

    # Check that the compiled binary exists, and is an executable.
    if [ -x mitgcmuv ]; then
        # create symbolic link to inputs and copy code
        cd ../run
        cp ../template/eedata .
        cp ../template/data.kpp .
        cp ../template/data.mnc .
        cp ../build/mitgcmuv .
    fi
)}


compile()
{
    # Note: both the 'run' and the 'build' directories are for the compilation/
    # running the simulation, and are not part of the code (therefore deletable).
    mkdir -p Hydrodynamics/run Hydrodynamics/build
    pushd Hydrodynamics/build > /dev/null

    # Variable that will point the genmake compiler to the correct options file
    # for a compilation on Daint systems.
    unset PARAMS
    if [[ ${PE_ENV} == "CRAY" ]]; then
        PARAMS=" -of ${MITGCM_ROOTDIR}/tools/build_options/linux_ia64_cray_archer"
	module load cray-netcdf
    fi

    # Check if we need MPI
    if [ "$MPI_OPTION" == 'True' ]; then
        PARAMS="$PARAMS -mpi"
    fi

    # Make sure that certain files necessary for compilation exist:
    array=('code/CPP_OPTIONS.h' \
           'code/EXF_OPTIONS.h' \
           'code/OBCS_OPTIONS.h' \
           'code/packages.conf' \
           'code/SIZE.h' \
           'template/data' \
           'template/data.exf' \
           'template/data.pkg' \
           'template/data.mnc' \
           'template/data.kpp' \
           'template/eedata' \
           'template/data.cal')
    for FILENAME in "${array[@]}"; do
        if [ ! -e ../"$FILENAME" ]; then
            printred "✗ Did not find $FILENAME" && exit 1
        fi
    done

    ARGSTRING="$MITGCM_ROOTDIR/tools/genmake2 -mods \
        ../code $MPI_PARAM -enable=mnc $PARAMS"
    $ARGSTRING > ../compilation.log

    # Check if Fortran Netcdf4 libraries were found
    if grep -q 'NetCDF-enabled binaries...  yes' ../compilation.log ; then
        printgreen '✓ netCDF4 libraries found.'
    else
        printred '✗ netCDF4 libraries NOT found!'
    fi

    make depend >> ../compilation.log 2>&1

    # Call the compilation script
    popd > /dev/null
    recompile >> Hydrodynamics/compilation.log

    # Check if the compilation was successful
    if grep -q 'INFO: Make exited without error' Hydrodynamics/compilation.log && \
    [ -x Hydrodynamics/run/mitgcmuv ]; then
        printgreen '✓ Compilation successful.'
    else
        printred '✗ Error in compilation, see <Hydrodynamics/compilation.log>'
    fi
}


# Execute the actual commands
case "$1" in
    'clean'         ) clean ;;
    'clean_runtime' ) clean_runtime ;;
    'bathymetry'    ) generate_bathymetry ;;
    'datafiles'     ) generate_datafiles ;;
    'compile'       ) compile ;;
    'recompile'     ) recompile ;;
    'all'           ) clean
                      generate_bathymetry
                      generate_datafiles
                      compile ;;
    ''              ) echo 'Need an input argument' ;;
    * ) printred "✗ Command not found: '${1}'" ;;
esac

