      INTEGER, PARAMETER :: sNx = !setSNX!
      INTEGER, PARAMETER :: sNy = !setSNY!
      INTEGER, PARAMETER :: OLx = 3
      INTEGER, PARAMETER :: OLy = 3
      INTEGER, PARAMETER :: nSx = !setNSX!
      INTEGER, PARAMETER :: nSy = !setNSY!
      INTEGER, PARAMETER :: nPx = !setNPX!
      INTEGER, PARAMETER :: nPy = !setNPY!
      INTEGER, PARAMETER :: Nx  = sNx*nSx*nPx
      INTEGER, PARAMETER :: Ny  = sNy*nSy*nPy
      INTEGER, PARAMETER :: Nr  = !setNR!
      INTEGER, PARAMETER :: MAX_OLX = OLx
      INTEGER, PARAMETER :: MAX_OLY = OLy

