#!/usr/bin/env pvpython

import paraview.simple as paraview_simple
from paraview.simple import Text, Show, Hide
from config_settings import startTime
import os
import pandas as pd

filename = '../Hydrodynamics/run/state.nc'

Variables = paraview_simple.NetCDFReader(FileName = filename) # pylint: disable=no-member

if not os.path.exists('Frames'):
    os.mkdir('Frames')

killZeros = paraview_simple.Threshold(Input = Variables) # pylint: disable=no-member
killZeros.Scalars = ['Temp']
killZeros.ThresholdRange = [0.1, 50]

Top20M = paraview_simple.Clip(Input = killZeros) # pylint: disable=no-member
Top20M.ClipType = 'Plane'
Top20M.ClipType.Origin = [0, 0, -50.0]
Top20M.ClipType.Normal = [0, 0, -1]

ScaleZ = paraview_simple.Transform(Input = Top20M) # pylint: disable=no-member
ScaleZ.Transform = 'Transform'
ScaleZ.Transform.Scale = [1.0, 1.0, 50.0]

renderView = paraview_simple.GetActiveViewOrCreate('RenderView')
ScaleZDisplay = paraview_simple.GetDisplayProperties(ScaleZ, view = renderView)
paraview_simple.ColorBy(ScaleZDisplay, ('POINTS', 'Temp'))

tempLUT = paraview_simple.GetColorTransferFunction('Temp')
tempLUT.RescaleTransferFunction(7, 20)

tempLUTColorBar = paraview_simple.GetScalarBar(tempLUT, renderView)
tempLUTColorBar.WindowLocation = 'AnyLocation'
tempLUTColorBar.Position = [0.25, 0.05]
tempLUTColorBar.ScalarBarLength = 0.4
tempLUTColorBar.Title = 'Temperature'
tempLUTColorBar.ComponentTitle = ''
tempLUTColorBar.Orientation = 'Horizontal'

tempLUTColorBar.TitleFontSize = 8
tempLUTColorBar.LabelFontSize = 8
tempLUTColorBar.ScalarBarThickness = 8
tempLUTColorBar.ScalarBarLength = 0.4
tempLUTColorBar.AddRangeLabels = 0

renderView.CameraPosition = [59386.9346, -48296.6730, 33048.0238]
renderView.CameraFocalPoint = [59386.8508, -48296.4718, 33047.8787]
renderView.CameraViewUp = [-0.1988, 0.5172468259474213, 0.8324]

TimeSteps = Variables.TimestepValues
N_Of_Steps = len(TimeSteps)

for i in range(N_Of_Steps):
    filename = f'Frames/Frame_{i:05}.png'
    renderView.ViewTime = TimeSteps[i]
    text = Text()
    time = startTime + pd.Timedelta(seconds = TimeSteps[i])
    tsmp = time.strftime("%d/%m/%Y, %H:%M UTC")
    text.Text = f'Lake Geneva top 50 meters temperature,\n{tsmp}'
    text1Display = Show(text)
    text1Display.WindowLocation = 'UpperCenter'
    text1Display.FontSize = 30
    paraview_simple.SaveScreenshot(filename, renderView, ImageResolution = [1920, 1080])
    Hide(text)
