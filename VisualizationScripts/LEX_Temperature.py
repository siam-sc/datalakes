'''
Plot the evolution of temperature.
'''

from netCDF4 import Dataset  # pylint: disable=no-name-in-module
from matplotlib import pyplot as plt, gridspec
import numpy as np
import glob
import pandas as pd
from scipy.interpolate import interp2d
from datetime import timezone

from PythonScripts.grid_tools import GridTools
from examples.MITgcm import insitu_data
from PythonScripts.config_settings import startTime

plt.rcParams.update({'xtick.labelsize': 7, 'ytick.labelsize': 7, 'axes.labelsize': 7})

inputPath = '../Hydrodynamics/run/'

# Function to locate LeXplore on the computational grid.
gridTools = GridTools(inputPath)
simData = insitu_data.ExtractSimulationData(gridTools)
indY = simData.indY_LeX
indX = simData.indX_LeX

tempData = {}
ncFile = glob.glob('')
ncFile = glob.glob(f'{inputPath}/mnc_*/state.0000000000.t030.nc')[0]
solnData = Dataset(ncFile)
Z = solnData['Z'][:].data
solnTimes = solnData['T'][:]
for i, time in enumerate(solnTimes):
    if time not in tempData.keys():
        tempData[int(time)] = solnData['Temp'][i, :, indY, indX].T.data

assert len(tempData) > 0, 'Could not fetch the data..'

# Now extract LeXplore data
LeX = insitu_data.extractLexplore()
LeX['LeX_Temp_18'] = np.nan
LeXdepths = [float(label[9:]) for label in LeX.columns]
LeX.index = [int((i.tz_convert(timezone.utc) - startTime).total_seconds())
             for i in LeX.index]
LeX_X, LeX_Y = np.meshgrid(LeX.index, LeXdepths)
LeXdata = LeX.to_numpy().T

times = sorted([time for time in list(tempData.keys())
                if LeX.index.max() >= time >= LeX.index.min()])
cutoff = np.where(Z < -90)[0][0]
Z = Z[0:cutoff]

Temp2D = np.zeros((cutoff, len(times)))
for i, time in enumerate(times):
    Temp2D[:, i] = tempData[time][0:cutoff]

SimToLeX = interp2d(times, -Z, Temp2D)(LeX.index, LeXdepths)

minVal = np.nanmin(LeXdata)
maxVal = np.nanmax(LeXdata)
valRange = np.exp(np.linspace(np.log(minVal), np.log(maxVal), 10))

days = [startTime + pd.Timedelta(seconds = i) for i in times]
X, Y = np.meshgrid(days, Z)

fig = plt.figure(figsize = (10, 10))
gridL = gridspec.GridSpec(2, 2)

ax0 = fig.add_subplot(gridL[0, 0])
im0 = ax0.contourf(X, Y, Temp2D, valRange, vmin = minVal, vmax = maxVal)
ax0.set_title('Model predictions', fontsize = 8)

ax1 = fig.add_subplot(gridL[1, 0])
ax1.contourf(LeX_X, -LeX_Y, LeXdata, valRange, vmin = minVal, vmax = maxVal)
ax1.set_title('LeX measurements', fontsize = 8)

ax3 = fig.add_subplot(gridL[:, 1])
im2 = ax3.contourf(LeX_X, -LeX_Y, LeXdata - SimToLeX, np.linspace(-8, 8, 17),
                   cmap = 'bwr')
ax3.set_title('Difference (in-situ - model)', fontsize = 8)

fig.colorbar(im0, ax = [ax0, ax1], shrink = 0.95, location = 'right', pad = 0.05)
fig.colorbar(im2, ax = [ax3], shrink = 0.95, location = 'right', pad = 0.05)

plt.savefig('Temp_LeX.png', dpi = 300, bbox_inches = 'tight', pad_inches = 0.05)
