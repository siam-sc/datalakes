'''
Plot the evolution of temperature.
'''

from netCDF4 import Dataset  # pylint: disable=no-name-in-module
from matplotlib import pyplot as plt

import numpy as np
import glob
import pandas as pd
from scipy.interpolate import interp2d

from PythonScripts.grid_tools import GridTools
from examples.MITgcm import insitu_data
from PythonScripts.config_settings import startTime

plt.rcParams.update({'xtick.labelsize': 7, 'ytick.labelsize': 7, 'axes.labelsize': 7})

inputPath = '../Hydrodynamics/run/'

# Function to locate LeXplore on the computational grid.
gridFileName = glob.glob(f'{inputPath}/grid*.nc')[0]
gridTools = GridTools(inputPath)
simData = insitu_data.ExtractSimulationData(gridTools)
indY = simData.indY_SHL2
indX = simData.indX_SHL2

# Load vertical spacing
with Dataset(gridFileName) as gridFile:
    Z = gridFile['Z'][:]

# Cycle through all the solution folders in time-ascending order
solnFolders = glob.glob(f'{inputPath}/result_*')

tempData = {}
for soln in solnFolders:
    solnFile = glob.glob(f'{soln}/pickup.*.nc')[0]
    solnData = Dataset(solnFile)
    solnTimes = solnData['T'][:]

    for i, time in enumerate(solnTimes):
        if time not in tempData.keys():
            tempData[int(time)] = solnData['Temp'][i, :, indY, indX].T.data

assert len(tempData) > 0, 'Could not fetch the data..'

times = sorted(list(tempData.keys()))

eliminate = np.where(tempData[int(time)] == 0)[0]
cutoff = eliminate[0] if len(eliminate) > 0 else len(tempData[int(time)])
Z = Z[0:cutoff]

Temp2D = np.zeros((cutoff, len(times)))
for i, time in enumerate(times):
    Temp2D[:, i] = tempData[time][0:cutoff]

days = [startTime + pd.Timedelta(seconds = i) for i in times]
X, Y = np.meshgrid(days, Z)

# Now extract SHL2 data
SHL2 = insitu_data.extractSHL2andGE3()[0]
SHL2days = [startTime + pd.Timedelta(seconds = i) for i in SHL2.index]
SHL2depths = [float(label[5:]) for label in SHL2.columns]
SHL2_X, SHL2_Y = np.meshgrid(SHL2days, SHL2depths)
SHL2data = SHL2.to_numpy().T
SimToSHL = interp2d(times, -Z, Temp2D)(SHL2.index, SHL2depths)

minVal = np.nanmin(SHL2data)
maxVal = np.nanmax(SHL2data)

fig, axs = plt.subplots(1, 4, figsize = (20, 10))
fig.tight_layout()

im0 = axs[0].pcolormesh(X, Y, Temp2D, shading = 'gouraud', vmin = minVal, vmax = maxVal)
axs[0].xaxis.set_major_locator(plt.MaxNLocator(2))
axs[0].set_title('Model predictions', fontsize = 8)

axs[1].pcolormesh(SHL2_X, -SHL2_Y, SHL2data, shading = 'gouraud',
                  vmin = minVal, vmax = maxVal)
axs[1].xaxis.set_major_locator(plt.MaxNLocator(2))
axs[1].set_title('SHL2 measurements', fontsize = 8)

im2 = axs[2].contourf(SHL2_X[11:, :], -SHL2_Y[11:, :], (SHL2data - SimToSHL)[11:, :], 20,
                      vmin = -0.5, vmax = 0.5, cmap = 'bwr')
axs[2].set_title('Deep layers difference (in-situ - model)', fontsize = 8)
axs[2].xaxis.set_major_locator(plt.MaxNLocator(2))

diffM = (SHL2data - SimToSHL)[:12, :]
uLim = abs(diffM).max()

im3 = axs[3].pcolormesh(SHL2_X[:12, :], -SHL2_Y[:12, :], (SHL2data - SimToSHL)[:12, :],
                        shading = 'gouraud', vmin = -uLim, vmax = uLim, cmap  = 'bwr')
axs[3].set_title('Surface layers difference (in-situ - model)', fontsize = 8)
axs[3].xaxis.set_major_locator(plt.MaxNLocator(2))

fig.colorbar(im0, ax = [axs[0], axs[1]], shrink = 0.95, location = 'bottom', pad = 0.05)
fig.colorbar(im2, ax = [axs[2]], shrink = 0.95, location = 'bottom', pad = 0.05)
fig.colorbar(im3, ax = [axs[3]], shrink = 0.95, location = 'bottom', pad = 0.05)

plt.savefig('Temp_SHL2.png', dpi = 300)
