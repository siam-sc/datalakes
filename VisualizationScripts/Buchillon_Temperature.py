'''
Plot the evolution of temperature.
'''

from netCDF4 import Dataset  # pylint: disable=no-name-in-module
from matplotlib import pyplot as plt
import numpy as np
import glob
import pandas as pd

from PythonScripts.grid_tools import GridTools
from examples.MITgcm import insitu_data
from PythonScripts.config_settings import startTime

plt.rcParams.update({'xtick.labelsize': 7, 'ytick.labelsize': 7, 'axes.labelsize': 7})

simData = pd.read_csv('CRpred.csv', index_col = 'time')
simData = simData[['T_1m_B', 'T_35m_B']]
simData.index = [startTime + pd.Timedelta(seconds = i) for i in simData.index]

# Now extract in-situ measurements
BuchillonData = insitu_data.extractBuchillonWaterTemp()
BuchillonData = BuchillonData[~BuchillonData.index.duplicated()]

fig, axs = plt.subplots(2, 2, figsize = (20, 10))

axs[0, 0].plot(BuchillonData.T_1m_B, label = 'in-situ')
axs[0, 0].plot(simData.T_1m_B, label = 'Datalakes')
axs[0, 0].set_title('Buchillon, 1m')

axs[0, 1].plot(BuchillonData.T_35m_B, label = 'in-situ')
axs[0, 1].plot(simData.T_35m_B, label = 'Datalakes')
axs[0, 1].set_title('Buchillon, 35m')

# Plot difference
difference = simData[simData.index.isin(BuchillonData.index)] - BuchillonData
axs[1, 0].plot(difference.T_1m_B, label = 'daily mean')
axs[1, 0].axhline(y = 0, color = 'k', linestyle = '--')
axs[1, 0].set_title('Difference (model - measurement) at 1m')

axs[1, 1].plot(difference.T_35m_B, label = 'daily mean')
axs[1, 1].axhline(y = 0, color = 'k', linestyle = '--')
axs[1, 1].set_title('Difference (model - measurement) at 35m')

for ax in axs.flatten():
    ax.set_ylabel('Temperature')
    ax.legend()

plt.savefig('Temp_Buchillon.png', dpi = 300, bbox_inches = 'tight', pad_inches = 0.05)
