from netCDF4 import Dataset  # pylint: disable=no-name-in-module
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd

from PythonScripts.grid_tools import GridTools
from examples.MITgcm import insitu_data
from examples.MITgcm.insitu_data import datasetLabels

plt.rcParams.update({'xtick.labelsize': 7, 'ytick.labelsize': 7, 'axes.labelsize': 7})

inputPath = '../Hydrodynamics/run/'
times = Dataset(f'{inputPath}/state.nc')['T']
# Function to locate Buchillon sensor on the computational grid.
gridTools = GridTools(inputPath)
extractData = insitu_data.ExtractSimulationData(gridTools)

# Load vertical spacing
with Dataset(f'{inputPath}/state.nc') as gridFile:
    Z = -gridFile['Z'][:].data

# Cycle through all the solution folders in time-ascending order
tempData = {}
for t, time in enumerate(times):
    Buchillon = extractData.extractBuchillonSimData(inputPath, t)
    SHL2 = extractData.extractSHL2SimData(inputPath, t)
    GE3 = extractData.extractGE3SimData(inputPath, t)
    LeXploreT = extractData.extractLeXSimData(inputPath, t)
    LeXploreVel = extractData.extractLeXVelSimData(inputPath, t)
    modelOutput = SHL2 + GE3 + LeXploreT + LeXploreVel + Buchillon + [np.nan, np.nan]
    tempData[int(time)] = pd.Series(dict(zip(datasetLabels, modelOutput)))

CRpred = pd.DataFrame(tempData).transpose()
CRpred.index.name = 'time'
CRpred.to_csv('CRpred.csv')
