'''
Plots the magnitude of the surface velocities for the solution files
'''

from matplotlib import pyplot as plt
import glob
import imageio
import numpy as np
from netCDF4 import Dataset # pylint: disable=no-name-in-module

inputPath = '../Hydrodynamics/run/'
outPath = '.'

depth = np.fromfile(glob.glob('../Hydrodynamics/binary_data/bathyGeneva_*m.bin')[0])
# Since the solution files can overlap, we take care to ensure that no snapshot
# is processed twice.
processedTimes = []
images = []

def timeToString(t):
    hour = int(np.floor(t/60))
    minute = int(t % 60)
    return f'{hour:02.0f}h:{minute:02.0f}m'

# Cycle through all the solution folders in time-ascending order
solnFolders = sorted(glob.glob(f'{inputPath}/state*.nc'))

for soln in solnFolders:
    variablesFile = Dataset(soln)
    time = variablesFile.variables['T'][:]

    for i in range(len(time)):
        cTime = int(time[i]/60)
        if cTime not in processedTimes:
            print(f'Processing minute: {timeToString(cTime)}')
            processedTimes.append(cTime)

            U = (variablesFile.variables['U'][i, 0, : , 1: ] +
                 variablesFile.variables['U'][i, 0, : , :-1] ) / 2
            V = (variablesFile.variables['V'][i, 0, 1: , : ] +
                 variablesFile.variables['V'][i, 0, :-1, : ] ) / 2

            UVabs = np.flipud(np.sqrt(U**2 + V**2)) + 0.1
            UVabs[np.flipud(depth.reshape(U.shape)) >= 0] = np.NaN
            image = plt.figure(figsize = (15, 8))

            plt.imshow(UVabs, aspect = 'equal',
                       extent = [0, 67.5, 0, 25.2])
            plt.colorbar(orientation = 'horizontal')
            plt.tight_layout()

            # Alternatively, can show a 'quiver' plot, but it does not produce
            # very nice visualizations.
            # plt.imshow(depth > 0, origin='lower')
            # plt.quiver(U[:,:], V[:,:], scale = 2)

            plt.title(f'Surface flow magnitude, t = {timeToString(cTime)}')

            plt.savefig('image.png', bbox_inches = 'tight')
            images.append(imageio.imread(f'{outPath}/image.png'))
            plt.close()

imageio.mimsave('FlowMagnitude.gif', images, fps = 10)
