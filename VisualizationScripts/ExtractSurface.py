# Collocates all surface variables for easier postprocessing.

from netCDF4 import Dataset

sourceData = Dataset('../Hydrodynamics/run/state.nc')
destData = Dataset('../Hydrodynamics/run/surface_colloc.nc', 'w')

for varName in ['T', 'X', 'Y']:
    destData.createDimension(varName, len(sourceData[varName]))
    destData.createVariable(varName, float, (varName))
    destData[varName][:] = sourceData[varName][:]
    destData[varName].setncatts(sourceData[varName].__dict__)

for varName in ['Temp', 'U', 'V']:
    destData.createVariable(varName, float, ('T', 'Y', 'X'))

destData['Temp'][:] = sourceData['Temp'][:, 0, :, :]
destData['U'][:] = (sourceData['U'][:, 0, :, 1: ] +
                    sourceData['U'][:, 0, :, :-1]) / 2
destData['V'][:] = (sourceData['V'][:, 0, 1: , :] +
                    sourceData['V'][:, 0, :-1, :]) / 2
destData.close()
