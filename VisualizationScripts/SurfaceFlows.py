#!/usr/bin/env pvpython

import paraview.simple as pimple
from paraview.simple import Text, Show, Hide
from config_settings import startTime
import os
import pandas as pd

inputFile = '../Hydrodynamics/run/surface_colloc.nc'

pimple.LoadDistributedPlugin('SurfaceLIC')

Variables = pimple.NetCDFReader(FileName = inputFile)

thresholdFilter = pimple.Threshold(Variables)
thresholdFilter.Scalars = ['POINTS', 'Temp']
thresholdFilter.ThresholdRange = [1, 30]

vectorData = pimple.Calculator(Input = thresholdFilter)
vectorData.Function = 'U*iHat+V*jHat'
vectorData.ResultArrayName = 'Velocity'

pimple.SetActiveSource(vectorData)
renderView = pimple.GetActiveViewOrCreate('RenderView')

# show data in view
vectorDataDisplay = pimple.Show(vectorData, renderView)

# change representation type
vectorDataDisplay.SetRepresentationType('Surface LIC')
pimple.ColorBy(vectorDataDisplay, ('POINTS', 'Velocity', 'Magnitude'))
vectorDataDisplay.NumberOfSteps = 40
vectorDataDisplay.StepSize = 0.5

renderView.CameraPosition = [34500, 12500, 75000]
renderView.CameraFocalPoint = [34500, 12500, 0]
renderView.ViewSize = [1280, 720]

velocityLUT = pimple.GetColorTransferFunction('Velocity')
velocityLUTColorBar = pimple.GetScalarBar(velocityLUT, renderView)
velocityLUTColorBar.AutoOrient = 0
velocityLUTColorBar.Orientation = 'Horizontal'
velocityLUTColorBar.WindowLocation = 'LowerCenter'
vectorDataDisplay.SetScalarBarVisibility(renderView, True)

glyphView = pimple.Glyph(Input = vectorData, GlyphType = 'Arrow')
glyphView.OrientationArray = ['POINTS', 'Velocity']
glyphView.ScaleArray = ['POINTS', 'Velocity']
glyphView.ScaleFactor = 20000
glyphView.GlyphMode = 'Uniform Spatial Distribution (Surface Sampling)'
glyphView.MaximumNumberOfSamplePoints = 200
glyph1Display = pimple.Show(glyphView, renderView)

scene = pimple.GetAnimationScene()
scene.UpdateAnimationUsingDataTimeSteps()

if not os.path.exists('SurfaceFrames'):
    os.mkdir('SurfaceFrames')

TimeSteps = Variables.TimestepValues
N_Of_Steps = len(TimeSteps)

for i in range(N_Of_Steps):
    filename = f'SurfaceFrames/SurfaceFlows_{i:05}.png'
    renderView.ViewTime = TimeSteps[i]
    text = Text()
    time = startTime + pd.Timedelta(seconds = TimeSteps[i])
    tsmp = time.strftime("%d/%m/%Y, %H:%M UTC")
    text.Text = f'Lake Geneva surface velocity,\n{tsmp}'
    text1Display = Show(text)
    text1Display.WindowLocation = 'UpperCenter'
    text1Display.FontSize = 30
    pimple.SaveScreenshot(filename, renderView, ImageResolution = [1920, 1080])
    Hide(text)
