'''
Plot the CFL statistics from the simulation results.
'''

from netCDF4 import Dataset  # pylint: disable=no-name-in-module
from matplotlib import pyplot as plt
import glob
import pandas as pd
import numpy as np

cfls = pd.DataFrame(columns = ['time', 'u_cfl', 'v_cfl', 'w_cfl'])

inputPath = '../Hydrodynamics/run/'

def timeToString(t):
    hour = int(np.floor(t/60))
    minute = int(t % 60)
    return f'{hour:02.0f}h:{minute:02.0f}m'

# Cycle through all the solution folders in time-ascending order
solnFolders = sorted(glob.glob(f'{inputPath}/result_*'),
                     key = lambda x : int(x.split('_')[-1]))

for soln in solnFolders:
    monitorFile = glob.glob(f'{soln}/monitor*.nc')[0]
    monitorStats = Dataset(monitorFile)

    CFLu = monitorStats['advcfl_uvel_max'][:]
    CFLv = monitorStats['advcfl_vvel_max'][:]
    CFLw = monitorStats['advcfl_wvel_max'][:]
    time = monitorStats['T'][:]

    for i in range(len(time)):
        cfls = cfls.append({'time'  : time[i],
                            'u_cfl' : CFLu[i],
                            'v_cfl' : CFLv[i],
                            'w_cfl' : CFLw[i]}, ignore_index = True)

assert len(cfls) > 0, 'Could not fetch the CFL numbers..'

cfls['time'] = cfls['time'].astype('int')
cfls = cfls.drop_duplicates(subset = 'time')

plt.figure()
plt.plot(cfls['time'] / 86400, cfls['u_cfl'], '--', color = 'black', label = 'U')
plt.plot(cfls['time'] / 86400, cfls['v_cfl'], '--', color = 'black', label = 'V')
plt.plot(cfls['time'] / 86400, cfls['w_cfl'], '.-', color = 'blue', label = 'W')
plt.legend()

plt.xlabel('Days')
plt.ylabel('Max CFL number')
plt.gca().set_ylim([0, 1.1*min(plt.gca().get_ylim()[1], 1)])

print(f'Max horizontal CFL is : {cfls[{"u_cfl", "v_cfl"}].max().max()}')
print(f'Max vertical CFL is   : {cfls["w_cfl"].max()}')

plt.savefig('CFLs.pdf', bbox_inches = 'tight')
plt.close()
