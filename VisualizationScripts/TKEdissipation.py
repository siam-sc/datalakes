'''
Plot the evolution of TKE.
'''

import os
from netCDF4 import Dataset  # pylint: disable=no-name-in-module
from matplotlib import pyplot as plt
from scipy.interpolate import UnivariateSpline, RegularGridInterpolator as RGI
import numpy as np
import glob
import pandas as pd
from MITgcmutils.mdjwf import densmdjwf as eqOfState

from PythonScripts.grid_tools import GridTools
from PythonScripts.interpolate_meteoswiss import getSupportPoints, extractData
import PythonScripts.config_settings as configs
from examples.MITgcm.insitu_data import ExtractSimulationData
from PythonScripts.global_parameters import HydrodynamicParameters

inputPath = '../Hydrodynamics/run/'

# Function to locate LeXplore on the computational grid.
gridFileName = glob.glob(f'{inputPath}/grid*.nc')[0]
gridTools = GridTools()
gridTools.init(inputPath)
simData = ExtractSimulationData()
simData.init(gridTools)
indY = simData.indY_SHL2
indX = simData.indX_SHL2
(SGx_SHL2, SGy_SHL2) = (534700, 144950)

# Load vertical spacing
with Dataset(gridFileName) as gridFile:
    Z = gridFile['Z'][:].data

# Cycle through all the solution folders in time-ascending order
solnFolders = glob.glob(f'{inputPath}/result_*')

U = {}
V = {}
Temp = {}
for soln in solnFolders:
    solnFile = glob.glob(f'{soln}/state.*.nc')[0]
    solnData = Dataset(solnFile)
    times = solnData['T'][:]

    for i, time in enumerate(times):
        if time not in U.keys():
            U[int(time)] = solnData['U'][i, :, indY, indX].T.data
            V[int(time)] = solnData['V'][i, :, indY, indX].T.data
            Temp[int(time)] = solnData['Temp'][i, :, indY, indX].T.data

assert len(U) > 0, 'Could not fetch the data..'
times = sorted(list(U.keys()))

cutoff = np.where(Z < -30)[0][0]
Z = Z[0:cutoff]

# Compute the dissipation rate of turbulent kinetic energy, in accordance with the
# formula in Wuest and Lorke.
nu = 1.307e-6
TKEdissip = np.zeros((cutoff, len(times)))
for i, time in enumerate(times):
    UVmag = np.sqrt(U[time][0:cutoff]**2 + V[time][0:cutoff]**2)
    dUpdZfn = UnivariateSpline(-Z, UVmag, s = 0, k = 2)
    dUpdZ = dUpdZfn.derivative(n = 1)(-Z)
    TKEdissip[:, i] = 7.5 * nu * pow(dUpdZ, 2)

TKEDmean = TKEdissip.mean(axis = 1)
TKEDupper = np.percentile(TKEdissip, 90, axis = 1, interpolation = 'nearest')
TKEDlower = np.percentile(TKEdissip, 10, axis = 1, interpolation = 'nearest')

# Extract weather forcing data at the location. For this we need to prepare the
# interpolating function and cycle through the entire meteoswiss dataset.
MSfile = f'{configs.FilePaths.MeteoSwissDataPath}/cosmo-e_eawag_2019010100.nc'
assert os.path.isfile(MSfile), f'Error: Meteoswiss file <{MSfile}> not found'
with Dataset(MSfile, mode = 'r') as fh:
    longitude = fh['lon_1'][50:80, 55:100]          # x-coord
    latitude = fh['lat_1'][50:80, 55:100]           # y-coord
sourcePts = getSupportPoints(latitude, longitude)[0]
interpolator = lambda data: RGI(sourcePts, data)((SGy_SHL2, SGx_SHL2))

surfaceForcing = {}
hourIndex = 0
currentT = 0
while currentT <= max(times):
    beginTime = configs.startTime + pd.Timedelta(seconds = currentT)
    MSdata = Dataset(f'{configs.FilePaths.MeteoSwissDataPath}/'
                     f'cosmo-e_eawag_{beginTime.strftime("%Y%m%d%H")}.nc')

    initH = 1 if hourIndex > 1 else 0
    for hour in range(initH, 13):
        # Extract tau and interpolate
        Ustress, Vstress = extractData(hour, 0, MSdata)[5:]
        tau = interpolator(np.sqrt(Ustress**2 + Vstress**2))
        surfaceForcing[currentT + 3600 * hour] = tau
    currentT += 12*3600

# Check for containment
assert surfaceForcing.keys() >= U.keys()

# Compute the expected TKE rate from the wind data.
eps_surface = {}
z = 10   # Height of wind above surface
k = 0.41 # Von Karman constant
g = 9.81
salinity = HydrodynamicParameters.salinityValue
with Dataset(gridFileName) as gridFile:
    dZ = -np.diff(gridFile['Zl'][:].data)

for i, time in enumerate(times):
    P = 0
    for _ in range(15):
        P = g * eqOfState(salinity, Temp[time][0], P) * dZ[0] / 2

    density = eqOfState(0.05, Temp[time][0], P)
    u_star = np.sqrt(surfaceForcing[time] / density)
    eps_surface[time] = u_star**3 / (k*z)
eps_vals = list(eps_surface.values())

days = [configs.startTime + pd.Timedelta(seconds = i) for i in times]

plt.rcParams.update({'xtick.labelsize': 7, 'ytick.labelsize': 7, 'axes.labelsize': 7,
                     'legend.fontsize': 7})
fig, axs = plt.subplots(1, 2)

axs[0].set_xscale('log')
axs[0].plot(TKEDmean, Z)
axs[0].fill_betweenx(Z, TKEDlower, TKEDupper, alpha = 0.2, color = 'red')
axs[0].set_title('TKE dissipation rate with 10%-90% CI', fontsize = 8)
axs[0].set_ylabel('Depth')

axs[1].semilogy(days, eps_vals, label = 'TKE from surface')
axs[1].semilogy(days, TKEdissip[0, :], label = 'TKE from hydrodynamics')
axs[1].semilogy([days[0], days[-1]], [np.mean(eps_vals)] * 2,
                label = 'Mean surface TKE')
axs[1].semilogy([days[0], days[-1]], [np.mean(TKEdissip[0, :])] * 2,
                label = 'Mean hydro TKE')
axs[1].legend()
axs[1].set_title('Surface TKE Dissipation', fontsize = 8)

plt.savefig('TKEdissipation_SHL2.png', dpi = 300)
