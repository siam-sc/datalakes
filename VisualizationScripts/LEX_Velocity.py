'''
Plot the evolution of temperature.
'''

from netCDF4 import Dataset  # pylint: disable=no-name-in-module
from matplotlib import pyplot as plt
import numpy as np
import glob
from scipy.interpolate import interp2d
from spectrum import pmtm

from PythonScripts.grid_tools import GridTools
from examples.MITgcm import insitu_data

plt.rcParams.update({'xtick.labelsize': 7, 'ytick.labelsize': 7, 'axes.labelsize': 7})

inputPath = '../Hydrodynamics/run/'

# Function to locate LeXplore on the computational grid.
gridFileName = glob.glob(f'{inputPath}/grid*.nc')[0]
gridTools = GridTools(inputPath)
simData = insitu_data.ExtractSimulationData(gridTools)
indY = simData.indY_LeX
indX = simData.indX_LeX

# Load vertical spacing
with Dataset(gridFileName) as gridFile:
    Z = gridFile['Z'][:]

# Cycle through all the solution folders in time-ascending order
solnFolders = glob.glob(f'{inputPath}/result_*')

Udata = {}
Vdata = {}
for soln in solnFolders:
    solnFile = glob.glob(f'{soln}/pickup.*.nc')[0]
    solnData = Dataset(solnFile)
    solnTimes = solnData['T'][:]

    for i, time in enumerate(solnTimes):
        if time not in Udata.keys():
            Udata[int(time)] = solnData['U'][i, :, indY, indX].T.data
            Vdata[int(time)] = solnData['V'][i, :, indY, indX].T.data

assert len(Udata) > 0, 'Could not fetch the data..'

# Now import LeXplore data
LeX = insitu_data.extractLexploreADCP()

times = sorted(LeX.index & Udata.keys())
vel2D = np.zeros((len(Z), len(times)))
for i, time in enumerate(times):
    vel2D[:, i] = np.sqrt(Udata[time]**2 + Vdata[time]**2)

SimToLeX = interp2d(times, -Z, vel2D)(times, insitu_data.LeXuvDepths)

for i in range(9):
    plt.subplot(3, 3, i+1)
    anData = LeX[LeX.columns[i]]
    anData = anData.dropna().values
    simData = SimToLeX[i, :]

    Sk_complex_data, weights_data, _ = pmtm(anData, NW = 4, method = 'unity')
    Sk_data = np.mean(abs(Sk_complex_data)**2 * weights_data, axis = 0)
    anDom = np.linspace(0.0, 1.0/(2.0*3600), len(Sk_data)//2)

    Sk_complex_sim, weights_sim, _ = pmtm(simData, NW = 4, method = 'unity')
    Sk_sim = np.mean(abs(Sk_complex_sim)**2 * weights_sim, axis=0)
    simDom = np.linspace(0.0, 1.0/(2.0*3*3600), len(Sk_sim)//2)

    plt.loglog(1/anDom/3600, Sk_data[0:len(anDom)] / np.sum(Sk_data))
    plt.loglog(1/simDom/3600, Sk_sim[0:len(simDom)] / np.sum(Sk_sim))
    plt.gca().invert_xaxis()

plt.savefig('LeX_PowerSpectrum.png', dpi = 300, bbox_inches = 'tight')
