'''
Plot the Brunt-Väisälä frequency N^2 - evolution over time and spreads.
'''

from netCDF4 import Dataset  # pylint: disable=no-name-in-module
from matplotlib import pyplot as plt
import numpy as np
import glob
import pandas as pd
from MITgcmutils.mdjwf import densmdjwf as eqOfState

from PythonScripts.grid_tools import GridTools
from PythonScripts.config_settings import startTime
from examples.MITgcm.insitu_data import ExtractSimulationData
from PythonScripts.global_parameters import HydrodynamicParameters

inputPath = '../Hydrodynamics/run/'

# Function to locate LeXplore on the computational grid.
gridFileName = glob.glob(f'{inputPath}/grid*.nc')[0]
gridTools = GridTools()
gridTools.init(inputPath)
simData = ExtractSimulationData()
simData.init(gridTools)
indY = simData.indY_SHL2
indX = simData.indX_SHL2

# Load vertical spacing
with Dataset(gridFileName) as gridFile:
    Z = gridFile['Z'][:].data
    dZ = -np.diff(gridFile['Zl'][:].data)

# Cycle through all the solution folders
solnFolders = glob.glob(f'{inputPath}/result_*')

tempData = {}
for soln in solnFolders:
    solnFileName = glob.glob(f'{soln}/state.*.nc')[0]
    solnFile = Dataset(solnFileName)
    times = solnFile['T'][:]

    for i, time in enumerate(times):
        if time not in tempData.keys():
            tempData[int(time)] = solnFile['Temp'][i, :, indY, indX].T.data

times = sorted(list(tempData.keys()))

eliminate = np.where(tempData[int(time)] == 0)[0]
cutoff = eliminate[0] if len(eliminate) > 0 else len(tempData[int(time)])
Nsqd = np.zeros((cutoff-1, len(times)))
g = 9.81

salinity = HydrodynamicParameters.salinityValue
for i, time in enumerate(times):
    Temp = tempData[time][:]

    PT = np.zeros(len(Z)) # Pressure at top of the cell
    PC = np.zeros(len(Z)) # Pressure at the center of the cell

    for _ in range(15):
        PC[0] = g * eqOfState(salinity, Temp[0], PC[0]) * dZ[0] / 2
    for j in range(1, len(Z)):
        for _ in range(15):
            PT[j] = PT[j-1] + g * eqOfState(salinity, Temp[j-1], PT[j]/10000) * dZ[j-1]
            PC[j] = PT[j] + g * eqOfState(salinity, Temp[j], PC[j]/10000) * dZ[j-1] / 2

    density = eqOfState(np.full(len(Z), salinity), Temp, PC/10000)[0:cutoff]
    density_prime = -np.diff(density)/np.diff(Z[0:cutoff])
    Nsqd[:, i] = (g / 9.998e+2) * density_prime

Z = Z[1:cutoff]

days = [startTime + pd.Timedelta(seconds = i) for i in times]
X, Y = np.meshgrid(days, Z)
plt.rcParams.update({'xtick.labelsize': 7, 'ytick.labelsize': 7, 'axes.labelsize': 7})

Nmean = Nsqd.mean(axis = 1)
Nupper = np.percentile(Nsqd, 90, axis = 1, interpolation = 'nearest')
Nlower = np.percentile(Nsqd, 10, axis = 1, interpolation = 'nearest')

fig, axs = plt.subplots(1, 2)

axs[0].set_xscale('log')
axs[0].plot(Nmean, Z)
axs[0].fill_betweenx(Z, Nlower, Nupper, alpha = 0.2, color = 'red')
axs[0].set_title('N^2 Mean with 10-90 percentiles')
axs[0].set_ylabel('Depth')

im1 = axs[1].pcolormesh(X, Y, np.log(Nsqd), vmin = -10, shading = 'gouraud')
axs[1].set_title('Brunt-Väisälä N^2')
axs[1].set_ylabel('Depth')
axs[1].xaxis.set_major_locator(plt.MaxNLocator(4))
fig.colorbar(im1, ax = [axs[1]], location = 'bottom')

plt.savefig('Nsqd_SHL2.png', dpi = 400)
