'''
Plots the magnitude of the surface velocities for the solution files
'''

from MITgcmutils import mnc
from netCDF4 import Dataset  # pylint: disable=no-name-in-module
from matplotlib import pyplot as plt
import glob, os
import imageio
from pandas import Timedelta
import numpy as np

from PythonScripts import grid_tools, interpolate_meteoswiss
from PythonScripts.config_settings import startTime, FilePaths

inputPath = '../Hydrodynamics/run/'
outPath = '../Hydrodynamics/run/'
MSindex = 0

# Extract bathymetry into 'Depth'
gridFile = mnc.mnc_files(f'{inputPath}grid*.nc')
depth = gridFile.variables['Depth'][:]
bd = depth > 0

# Since the solution files can overlap, we take care to ensure that no snapshot
# is processed twice.
processedTimes = []
images = []

# Cycle through all the solution folders in time-ascending order
solnFolders = sorted(glob.glob(f'{inputPath}/result_*'),
                     key = lambda x : int(x.split('_')[-1]))

firstF = f'{FilePaths.MeteoSwissDataPath}/cosmo-e_eawag_2019010100.nc'
with Dataset(firstF, mode = 'r') as fh:
    longitude = fh['lon_1'][50:80, 55:100]          # x-coord
    latitude = fh['lat_1'][50:80, 55:100]           # y-coord

_, _, relAngle = interpolate_meteoswiss.getSupportPoints(latitude, longitude)
SGx, SGy = grid_tools.LatLonToSwissGrid(latitude, longitude)
xSG, ySG = grid_tools.GridTools().SwissGridToCompGrid(SGx, SGy)

xPts = gridFile.variables['X'][:]
yPts = gridFile.variables['Y'][:]

xMax = max(xPts)
yMax = max(yPts)
filterMesh = (xSG > 0) & (xSG < xMax) & (ySG > 0) & (ySG < yMax)

(SGx_B, SGy_B) = (520160, 145759) # Buchillon station coordinates
xBuchillon, yBuchillon = grid_tools.GridTools().SwissGridToCompGrid(SGx_B, SGy_B)

X, Y = np.meshgrid(xPts, yPts)

for soln in solnFolders:
    variablesFile = mnc.mnc_files(f'{soln}/state*.nc')
    time = variablesFile.variables['T'][:]

    for i in range(len(time)):
        cTime = int(time[i]/60/60)
        if cTime not in processedTimes:
            fDate = (startTime+Timedelta(seconds = time[i]))
            print(f'Processing : {fDate}')
            hour = fDate.hour if fDate.hour < 13 else fDate.hour-12

            fDate2 = fDate.replace(hour = fDate.hour - fDate.hour % 12)
            fDateStr = fDate2.strftime("%Y%m%d%H")

            MSfile = f'{FilePaths.MeteoSwissDataPath}/cosmo-e_eawag_{fDateStr}.nc'
            if os.path.exists(MSfile):
                with Dataset(MSfile, mode = 'r') as fh:
                    Ums = fh['U_10M'][hour, MSindex, 50:80, 55:100]
                    Vms = fh['V_10M'][hour, MSindex, 50:80, 55:100]
            else:
                fDateStr = (fDate - Timedelta(hours = 12)).strftime("%Y%m%d%H")
                MSfile = f'{FilePaths.MeteoSwissDataPath}/cosmo-e_eawag_{fDateStr}.nc'
                with Dataset(MSfile, mode = 'r') as fh:
                    Ums = fh['U_10M'][12, MSindex, 50:80, 55:100]
                    Vms = fh['V_10M'][12, MSindex, 50:80, 55:100]

            Urotated = np.cos(relAngle)*Ums - np.sin(relAngle)*Vms
            Vrotated = np.sin(relAngle)*Ums + np.cos(relAngle)*Vms

            processedTimes.append(cTime)

            U = (variablesFile.variables['U'][i, 0, : , 1: ] +
                 variablesFile.variables['U'][i, 0, : , :-1] ) / 2
            V = (variablesFile.variables['V'][i, 0, 1: , : ] +
                 variablesFile.variables['V'][i, 0, :-1, : ] ) / 2

            plt.subplot(111, aspect = 'equal')
            plt.contourf(X, Y, bd, alpha = 0.25, levels = [0, 0.5], colors = 'gray')
            plt.quiver(xSG[filterMesh], ySG[filterMesh], Urotated[filterMesh],
                       Vrotated[filterMesh],
                       color = 'blue', alpha = 0.6, width = 0.003)
            plt.quiver(X[bd], Y[bd], U[bd], V[bd], scale = 2)
            plt.plot(xBuchillon, yBuchillon, 'o', color = 'red', markersize = 3)

            plt.title(f'Wind/Surface currents, t = {fDate}')
            plt.xlim([0, xMax])
            plt.ylim([0, yMax])

            destName = f'{outPath}/image_.{fDate}.png'
            plt.savefig(destName, bbox_inches = 'tight', dpi = 200)
            images.append(imageio.imread(destName))
            plt.close()

imageio.mimsave('FlowMagnitude.gif', images, fps = 3)
