import glob, os
import numpy as np
from pandas import Timedelta, read_csv

from config_settings import meshSize, FilePaths, startTime, endTime
from PythonScripts.auxiliary_functions import modifyArguments
from PythonScripts.global_parameters import GridParams as gridP, Discretization
meshP = Discretization[meshSize]


def cellFlow(maxH, h1, h2, totalFlow):
    '''
    Flow through a particular cell based on the logarithmic law of turbulent
    boundary layers, as per `Modelling Aquatic Ecosystems` by Reichert,
    Mieleitner and Schuwirth.
    '''

    kappa = 0.4
    S_0 = 0.01
    u_star = np.sqrt(9.8*maxH*S_0)

    if h1 > 0:
        return (totalFlow*(h2/maxH) + (u_star/kappa)*h2*np.log(h2/maxH) -
                totalFlow*(h1/maxH) - (u_star/kappa)*h1*np.log(h1/maxH))
    else:
        return totalFlow*(h2/maxH) + (u_star/kappa)*h2*np.log(h2/maxH)


def vectorFlow(totalFlow, h):
    '''
    Computes the entire flow profile with the zeroth cell corresponding to the
    top layer.
    '''

    n = len(h) - 1
    flows = np.zeros(n)
    for i in range(n):
        flows[n-i-1] = cellFlow(h[n], h[i], h[i+1], totalFlow) / (h[i+1]-h[i])
    return flows


def writeSingleDay(day, binaryDataPath, finalRecord):
    '''
    Helper function to process a single day of data.
    '''

    data2009 = read_csv(f'{FilePaths.BafuDataPath}/station_2009/Year_{day.year}.csv',
                        index_col = 'Date_Time', parse_dates = True)
    data2606 = read_csv(f'{FilePaths.BafuDataPath}/station_2606/Year_{day.year}.csv',
                        index_col = 'Date_Time', parse_dates = True)

    # Variable arrays to be filled and stored
    southV    = np.zeros((meshP.verticalN, meshP.minX))
    southTemp = np.zeros((meshP.verticalN, meshP.minX))
    westU     = np.zeros((meshP.verticalN, meshP.minY))
    westTemp  = np.zeros((meshP.verticalN, meshP.minY))

    # Find the range of vertical cells through which the river flows.
    depth = [sum(meshP.verticalSpacing[0:i]) for i in range(1, meshP.verticalN+1)]
    max2009 = np.where(np.array(depth) > gridP.depth2009)[0][0]
    max2606 = np.where(np.array(depth) > gridP.depth2606)[0][0]

    heights2009 = np.insert(depth[0:(max2009+1)], 0, 0)
    heights2606 = np.insert(depth[0:(max2606+1)], 0, 0)

    # Output files
    southVfile = open(f'{binaryDataPath}/BC_south_v.bin', 'ab')
    southTfile = open(f'{binaryDataPath}/BC_south_T.bin', 'ab')
    westUfile = open(f'{binaryDataPath}/BC_west_u.bin', 'ab')
    westTfile = open(f'{binaryDataPath}/BC_west_T.bin', 'ab')

    nHours = 25 if finalRecord else 24
    for i in range(nHours):
        time = day + Timedelta(hours = i)
        ind2009 = data2009.index.get_loc(time, method = 'nearest')
        ind2606 = data2606.index.get_loc(time, method = 'nearest')

        flow2009 = vectorFlow(data2009.iloc[ind2009]['Flow'], heights2009)
        flow2606 = -vectorFlow(data2606.iloc[ind2606]['Flow'], heights2606)
        temp2009 = data2009.iloc[ind2009]['Temperature']

        westU[0:(max2606+1), meshP.RiverIndices[0][2]-1] = flow2606 / meshSize
        southV[0:(max2009+1), meshP.RiverIndices[3][1]-1] = flow2009 / meshSize
        southTemp[0:(max2009+1), meshP.RiverIndices[3][1]-1] = temp2009

        # This is the outflow temperature through Geneva. Since it is flowing out,
        # it does not affect the interior, so we can just set it to whatever.
        westTemp[0:(max2606+1), meshP.RiverIndices[0][2]-1] = temp2009

        southV.tofile(southVfile)
        westU.tofile(westUfile)
        southTemp.tofile(southTfile)
        westTemp.tofile(westTfile)

    southVfile.close()
    westUfile.close()
    southTfile.close()
    westTfile.close()


def GenerateRiverData(binaryDataPath):
    '''
    Generates input river data from the BAFU measurements. Here we use station
    IDs (as per the labeling from hydrodaten.admin.ch) to distinguish between
    different rivers. In case there is no data for the requested time, then the
    closest record is searched for.
    '''

    for fileName in glob.glob(f'{binaryDataPath}/BC_'):
        os.remove(fileName)

    day = startTime
    while day < endTime:
        if day + Timedelta(days = 1) >= endTime:
            writeSingleDay(day, binaryDataPath, finalRecord = True)
        else:
            writeSingleDay(day, binaryDataPath, finalRecord = False)
        day += Timedelta(days = 1)


def updateInputFileOBCS(outputFolder):
    '''
    Updates MITgcm configuration file `data.exf` to have the correct
    starting day and hour for the surface forcing terms.
    '''

    modifyDataEXF = f'{outputFolder}/data.exf'
    with open(modifyDataEXF, 'r') as file:
        dataIn = file.readlines()

    with open(modifyDataEXF, 'w') as dataOut:
        for line in dataIn:
            if 'startdate1' in line and 'obcs' in line:
                loc = line.find('=')
                dataOut.write(f'{line[0:loc]}= {startTime.strftime("%Y%m%d")}\n')
            else:
                dataOut.write(line)


def updateInputPathsOBCS(riverDataPath, outputFolder):
    '''
    Update the paths for the interpolated OBCS binaries to point to the correct directory.
    '''

    varMap = {'OBSvFile' : f'"{riverDataPath}/BC_south_v.bin"',
              'OBSTFile' : f'"{riverDataPath}/BC_south_T.bin"',
              'OBWuFile' : f'"{riverDataPath}/BC_west_u.bin"',
              'OBWTFile' : f'"{riverDataPath}/BC_west_T.bin"'}

    modifyDataEXF = f'{outputFolder}/data.obcs'
    modifyArguments(varMap, modifyDataEXF, modifyDataEXF)
