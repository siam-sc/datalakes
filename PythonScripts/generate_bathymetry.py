'''
Generates a fairly tight rectangular bathymetry of Lake Geneva for MITgcm, with
mesh size as one of the input arguments. As MITgcm requires that the domain
decomposition of the grid produces tiles of equal size, the dimensions of the
grid must therefore satisfy some divisibility criterion: for instance, if we
want to use all 36 processors per node on the Daint multi-core machines, the
number of cells in the horizontal grid must be divisible by 36. We do not
determine what would constitute compatible mesh parameters here, and therefore
expect the global dimensions of the grid to be provided as well. See the
`global_parameters.py` file for example configurations based on mesh size. For
the supported mesh sizes, the grids have been precomputed and stored in the
`PythonScripts/template_bathymetry/` folder. If this script has been called,
then it means that a precomputed bathymetry has not been found.
'''

import os
import numpy as np
from scipy.interpolate import RegularGridInterpolator as RGI

from config_settings import meshSize, FilePaths as paths
from grid_tools import GridTools
from global_parameters import GridParams as gridP, Discretization
meshP = Discretization[meshSize]


# Copy topography settings from the global_parameters file.
leftX = gridP.GenevaRasterLeftX
leftY = gridP.GenevaRasterLeftY
minX = meshP.minX
minY = meshP.minY
distY = gridP.distY
cellSize = gridP.GenevaRasterCellSize

print('Interpolating the topography for the lake:')

# Import bathymetry and shift height so that the surface of the lake is at a
# height of zero.
RasterFile = paths.GridRasterFile
assert os.path.isfile(RasterFile), 'Raster file not found! Aborting'
bathymetry = np.flipud(np.loadtxt(RasterFile)) - gridP.GenevaRasterHeightASL
print('    Raster topography imported...')

ncols, nrows = bathymetry.shape

# We artificially create a barrier around the shoreline so that if the water
# level rises, then it rises vertically, and does not 'spill out'.
bathymetry = np.minimum(bathymetry, np.zeros([ncols, nrows]))
bathymetry += 2 * (bathymetry > -0.01)

# Generate the support points for the original bathymetry.
sourceX = np.arange(leftX, leftX+nrows*cellSize, cellSize)
sourceY = np.arange(leftY, leftY+ncols*cellSize, cellSize)

# Make sure that the spatial extent of the new bathymetry is bigger than the
# dimensions of the imported rasterization. otherwise we might end up with a
# situation where a part of the lake is outside the grid domain.
distX = np.sqrt((gridP.Y1-gridP.Y0)**2 + (gridP.X1-gridP.X0)**2)
if (minX*meshSize < distX) or (minY*meshSize < distY):
    if (minX*meshSize < distX):
        print('Condition minX*meshSize < distX is violated!')
    if (minY*meshSize < distY):
        print('Condition minY*meshSize < distY is violated!')
    raise Exception('Please fix the grid size parameters')

# Now the mesh will be generated. Some statistics:
print(f'    Computational grid origin     : ({gridP.X0:.1f}, {gridP.Y0:.1f})\n'
      f'    Horizontal grid size          : {meshSize*minX}m\n'
      f'    Vertical grid size            : {meshSize*minY}m')

# Generate a grid of support points with southwest vertex at the origin, then
# rotate and shift them to Swiss grid coordinates.
x, y = np.meshgrid(np.arange(minX)*meshSize, np.arange(minY)*meshSize)
xSG, ySG = GridTools().CompGridToSwissGrid(x, y)

# Create the interpolating function. Since the target mesh might not be entirely
# contained within the original topography domain (due to the rotation), we do
# not check if a requested point is out-of-bounds; thus the input topography
# must contain the entire lake.
interpolator = RGI(points = [sourceY, sourceX],
                   values = bathymetry,
                   bounds_error = False, fill_value = 2)

# Do the thing
CompGrid = np.zeros([minY, minX])
for i in range(minY):
    pts = np.stack((ySG[i], xSG[i]), axis=-1)
    CompGrid[i] = interpolator(pts)

# Modify bathymetry. To ensure that the river flows are computed correctly, we
# slightly increase the depth of those relevant cells. In some cases we need to
# extend the artificial river channel so that the velocity variables, which are
# centered at the edges of the cells, can propagate the flows.
RI = meshP.RiverIndices

# Because MITgcm is implemented so poorly, we have no choice but to manually
# handle the rivers for each discretization.
if meshSize == 1000:
    CompGrid[RI[0][2]-1, RI[0][1]-1] = -5
    CompGrid[RI[3][2]-1, RI[3][1]-1] = -5
elif meshSize in [100, 200, 450]:
    CompGrid[RI[0][2]-1, RI[0][1]-1] = -gridP.depth2606 - 0.2
    CompGrid[RI[3][2]-1, RI[3][1]-1] = -gridP.depth2009 - 0.2

if meshSize == 200 or meshSize == 100:
    CompGrid[RI[0][2]-1, RI[0][1]] = -gridP.depth2606 - 0.2
    CompGrid[RI[3][2], RI[3][1]-1] = -gridP.depth2009 - 0.2

print('Computational grid generated')

output = open(f'template_bathymetry/bathyGeneva_{meshSize}m.bin', 'wb')
CompGrid.tofile(output)
output.close()
