'''
A script to launch a deterministic simulation of lake Geneva with restarts in
accord with chosen parameters in `Scripts/config_settings.py`.
'''

import os, shutil, glob
import ctypes
from pandas import Timedelta

import config_settings as configs
import auxiliary_functions as aux
from global_parameters import Discretization
from grid_tools import GridTools

import interpolate_meteoswiss as interpMS
import generate_river_data as interpRD
from process_height import ManageWaterLevels

# Monthly Secchi depth measurements from SHL2
monthlySecchiD = [13.8, 15.5, 9.3, 7.0, 5.15, 8.8, 8.15, 3.7, 6.6, 8.55, 8.1, 10.5]

# Load the executable
os.chdir('../Hydrodynamics/run/')
MITgcmExec = ctypes.CDLL('./mitgcmuv')
thid = ctypes.byref(ctypes.c_int(1))
MITgcmExec.eeboot_()

# Determine number of processors and timestep
meshP = Discretization[configs.meshSize]
nProcs = meshP.nSx*meshP.nSy*meshP.nPx*meshP.nPy

# Initialize necessary classes
gridData = GridTools()
waterLevel = ManageWaterLevels(verbosity = 1)

# The initial iteration is not guaranteed to be zero in case this is a restart
# or a simulation on Daint.
currentT = int(aux.extractVariable('startTime', 'data'))
currentIter = int(aux.extractVariable('nIter0', 'data'))

# Make sure there are actually pickup files in the run directory; at least one
# should be there.
if configs.initialCond or currentT > 0:
    if not os.path.isfile(f'pickup.{currentIter:010d}.t001.nc'):
        raise FileNotFoundError('No pickup file found!')

currentTime = configs.startTime + Timedelta(seconds = currentT)
print(f'Starting time: {currentTime}')

if currentTime >= configs.endTime:
    raise ValueError('The target date has already been reached in the simulation')

# Time to simulation until pickup file
endT = currentT + 3600

# Prepare atmospheric forcing data
MSindex = 0
MSdir = f'{configs.FilePaths.ScratchPath}/R{configs.meshSize}'
os.makedirs(f'{MSdir}/E_{MSindex}', exist_ok = True)
interpMS.updateInputPaths(MSdir, '.', MSindex)

if configs.withRivers:
    riverDataPath = f'{configs.FilePaths.ScratchPath}/OBCS{configs.meshSize}'
    os.makedirs(riverDataPath, exist_ok = True)
    interpRD.updateInputFileOBCS('.')
    interpRD.updateInputPathsOBCS(riverDataPath, '.')

# Set to 'True' to interpolate input fields
if False:
    interpMS.interpolate(MSdir, MSindex)
    if configs.withRivers:
        interpRD.GenerateRiverData(riverDataPath)

aux.updateInputFiles(currentT, endT, currentIter, 'data', setPickup = configs.initialCond)

# Launch the simulations
while(currentTime < configs.endTime):
    # Delete unnecessary files leftover from previous simulations.
    aux.removeFiles(['grid.*.nc'], '.')

    secchiD = ctypes.byref(ctypes.c_double(monthlySecchiD[currentTime.month - 1]))
    endIter = currentIter + (endT - currentT) // meshP.deltaT

    # Execute MITgcm code
    MITgcmExec.the_model_main_(thid, secchiD)

    # Update current and next simulation times; also update a number of input parameters
    # for the next simulation.
    aux.moveFiles(['pickup.0*.nc'], '.', f'result_{currentIter:010d}')
    currentIter = endIter
    currentT = endT
    currentTime = configs.startTime + Timedelta(seconds = currentT)
    endT = currentT + 3600

    aux.printGreen(f'Current simulation time: {currentTime}')

    aux.updateInputFiles(currentT, endT, currentIter, 'data')

    # Check if grid info needs to be fetched.
    if not gridData.gridFetched:
        gridData.init('.')
        if configs.withRivers:
            waterLevel.init(gridData)

    # Adjust water levels
    if configs.withRivers:
        readings = waterLevel.extractWaterLevel(currentT)
        waterLevel.adjustWaterLevel('.', currentT, readings)

    # Generate a folder for results
    outFolder = f'result_{currentIter:010d}'
    shutil.rmtree(outFolder, ignore_errors = True)
    os.mkdir(outFolder)

    # Rename restart files so that they are picked up by MITgcm.
    for pickupFile in glob.glob('pickup.ckptA.*.nc'):
        newpickup = pickupFile.replace('ckptA', f'{currentIter:010d}')
        os.rename(pickupFile, newpickup)

for filename in glob.glob('pickup.0*.nc'):
    shutil.copy(filename, f'result_{currentIter:010d}')

os.chdir('../../PythonScripts/')
