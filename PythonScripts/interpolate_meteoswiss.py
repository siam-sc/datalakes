'''
Scripts to interpolate MeteoSwiss ensemble data from the COSMO-E model onto the
MITgcm grid for the duration of the entire simulation.

Input arguments:
outputFolder  - output directory for the generated data
MSindex       - index of the MeteoSwiss ensemble member

Output variables:
  T   - temperature at 2m above surface
  U,V - horizontal, vertical (with respect to the grid) components of wind
        velocity at 10m above surface
  P   - surface pressure
  SH  - specific humidity computed from temperature and relative humidity
  SWR - short-wave radiation
  LWR - long-wave radiation
'''

import numpy as np
import os
from datetime import datetime, timezone
from pandas import Timedelta
from scipy.interpolate import RegularGridInterpolator
from netCDF4 import Dataset  # pylint: disable=no-name-in-module
import scipy.signal

from config_settings import meshSize, FilePaths, startTime, endTime
from PythonScripts.grid_tools import GridTools, LatLonToSwissGrid
from PythonScripts.auxiliary_functions import modifyArguments
from PythonScripts.global_parameters import GridParams as gridP, Discretization
meshP = Discretization[meshSize]


def getSupportPoints(latitude, longitude):
    '''
    This function converts latitude/longitude coordinates from MeteoSwiss
    data into a regularly gridded data compatible with the computational
    domain. The purpose of this function is to enable the use of
    scipy.interpolate.RegularGridInterpolator to perform the interpolation
    (for speed reasons). The issue here is that RGI requires the x and y
    support points to be specified as 1D vectors such that their cartesian
    product would generate the mesh. To work around this issue, we rotate
    and shift the MeteoSwiss grid to be upright w.r.t. the coordinate
    system.
    '''

    gridT = GridTools()
    lonMS, latMS = LatLonToSwissGrid(latitude, longitude)

    lonMScorner = lonMS[0, 0]
    latMScorner = latMS[0, 0]
    lonMS -= lonMScorner
    latMS -= latMScorner
    angleMS = -np.arctan2(latMS[0, -1], lonMS[0, -1])
    lonMSrotated = np.cos(angleMS)*lonMS - np.sin(angleMS)*latMS + lonMScorner
    latMSrotated = np.sin(angleMS)*lonMS + np.cos(angleMS)*latMS + latMScorner

    # We also need to rotate the numerical grid by the same angle.
    lonG, latG = np.meshgrid(np.arange(meshP.minX) * meshSize,
                             np.arange(meshP.minY) * meshSize)

    angle = gridT.gridAngle + angleMS
    lonGrotated = np.cos(angle)*lonG - np.sin(angle)*latG + gridP.X0 + meshSize/2
    latGrotated = np.sin(angle)*lonG + np.cos(angle)*latG + gridP.Y0 + meshSize/2

    # Small heuristic correction for error introduced by the
    # spherical->rectangular transformation.
    lonMSrotated += (latMSrotated - latMScorner) * (18/64476.414)
    lonGrotated  += (latGrotated - latMScorner) * (18/64476.414)

    # Create the interpolating function
    sourcePts = (latMSrotated[:, 0], lonMSrotated[0, :])
    destPts = np.stack((latGrotated.flatten(), lonGrotated.flatten())).T

    return (sourcePts, destPts, angleMS-angle)


def computeC10(windSpeedSqrd):
    '''
    Computes the C10 coefficient in accordance with Wuest and Lorke. To make the function
    relatively continuous, a transition point is chosen where the low wind speed
    coefficient is approximately equal to the high speed coefficient.
    '''

    transitionpt = 15.21
    if windSpeedSqrd > transitionpt:
        C10 = 0.007
        for _ in range(4):
            C10 = pow((1/0.41) * np.log(10*9.81 / (C10*windSpeedSqrd)) + 11.3, -2)
    else:
        C10 = 0.0044 * pow(windSpeedSqrd, -1.15/2)
    return C10


def extractData1_aux(hour, MSdata, MSindex, forRS = False):
    Temp = MSdata['T_2M'][hour, MSindex, 50:80, 55:100]       # Temperature, in K
    U = MSdata['U_10M'][hour, MSindex, 50:80, 55:100]         # Eastward wind
    V = MSdata['V_10M'][hour, MSindex, 50:80, 55:100]         # Northward wind
    PMSL = MSdata['PMSL'][hour, MSindex, 50:80, 55:100]       # Pressure at sea level
    HUM = MSdata['RELHUM_2M'][hour, MSindex, 50:80, 55:100]   # Relative humidity index
    CLCT = MSdata['CLCT'][hour+1, MSindex, 50:80, 55:100]/100 # Fractional cloud cover
    SWR = MSdata['GLOB'][hour+1, MSindex, 50:80, 55:100]      # Short-wave radiation

    # Sometimes cloud cover is slightly negative ..
    CLCT[CLCT < 0] = 0

    # Using https://www.sandhurstweather.org.uk/barometric.pdf to convert mean sea level
    # pressure to pressure at the lake's height.
    P = PMSL / np.exp(gridP.GenevaRasterHeightASL / (29.263*Temp))

    # Convert wind to surface stress in accordance with `Small scale hydrodynamics in
    # lakes` by Wuest and Lorke.
    if not forRS:
        rho_air = P / (287.058 * Temp)
        C10 = np.zeros(U.shape)
        windSpeedSqrd = U**2 + V**2
        for (i, j), _ in np.ndenumerate(U):
            C10[i, j] = computeC10(windSpeedSqrd[i, j])

        Ustress = rho_air * C10 * np.sqrt(windSpeedSqrd) * U
        Vstress = rho_air * C10 * np.sqrt(windSpeedSqrd) * V

    # Compute specific humidity from the relative value; based on
    # formula from https://earthscience.stackexchange.com/a/2361
    vaporPressure = 6.11 * np.exp(17.67 * (Temp-273.15) / (Temp-29.65)) # in units of hPa
    SH = (HUM*vaporPressure) / (0.263*6.11*P)

    # Estimate downward radiation based on Alfred Wüest's course Limnology, chapter 5.
    A_L = 0.03   # Infrared radiation albedo
    a = 1.09     # Calibration parameter
    E_a = a * (1 + 0.17 * np.power(CLCT, 2)) * 1.24 * np.power(vaporPressure / Temp, 1./7)
    LWR = (1 - A_L) * 5.67e-8 * E_a * np.power(Temp, 4)

    if forRS:
        return Temp, SWR, LWR, HUM, CLCT, U, V
    else:
        return Temp, P, SWR, LWR, SH, Ustress, Vstress


def extractData1(currentDate, fileNames, MSindex):
    dtstr = lambda day : day.strftime("%Y%m%d%H")
    MSfile = f'{FilePaths.MeteoSwissDataPath}/E1/cosmo-e_eawag_{dtstr(currentDate)}.nc'
    assert os.path.isfile(MSfile), \
        f'Error: required Meteoswiss file <{MSfile}> not found'
    MSdata = Dataset(MSfile, mode = 'r')

    # Extract support points for the MeteoSwiss data on the computational grid.
    # Also extract the grid points with respect to the center of each cell.
    longitude = MSdata['lon_1'][50:80, 55:100]          # x-coord
    latitude = MSdata['lat_1'][50:80, 55:100]           # y-coord

    sourcePts, destPts, relAngle = getSupportPoints(latitude, longitude)

    # Generate destination points for the U and V coordinates
    destPtsU = destPts.copy()
    destPtsV = destPts.copy()
    destPtsU[:, 0] -= meshSize/2
    destPtsV[:, 1] -= meshSize/2
    ptsList = [destPts, destPts, destPts, destPts, destPts, destPtsU, destPtsV]

    initH = 1 if currentDate > startTime else 0
    for hour in range(initH, 13):
        Temp, P, SWR, LWR, SH, Su, Sv = extractData1_aux(hour, MSdata, MSindex)

        # Rotate Meteoswiss data to align with the computational grid.
        SuRotated = np.cos(relAngle)*Su - np.sin(relAngle)*Sv
        SVRotated = np.sin(relAngle)*Su + np.cos(relAngle)*Sv

        # Convert the entire netCDF dataset
        varList = [Temp, P, SWR, LWR, SH, SuRotated, SVRotated]
        for i in range(len(varList)):
            output = open(fileNames[i], 'ab')

            interpolator = RegularGridInterpolator(sourcePts, varList[i])
            data = interpolator(ptsList[i])
            data.tofile(output)
            output.close()


def extractData2_aux(hour, MSdata, seed, forRS = False):
    np.random.seed(seed)

    Temp = MSdata['T_2M_MEAN'][hour, 50:80, 55:100]       # Temperature, in K
    U = MSdata['U_MEAN'][hour, 0, 50:80, 55:100]          # Eastward wind
    V = MSdata['V_MEAN'][hour, 0, 50:80, 55:100]          # Northward wind
    PMSL = MSdata['PMSL_MEAN'][hour, 50:80, 55:100]       # Pressure at sea level
    HUM = MSdata['RELHUM_2M_MEAN'][hour, 50:80, 55:100]   # Relative humidity index
    CLCT = MSdata['CLCT_MEAN'][hour+1, 50:80, 55:100]/100 # Fractional cloud cover
    SWR = MSdata['GLOB_MEAN'][hour+1, 50:80, 55:100]      # Short-wave radiation

    # Perturb the windfield by a random spatially correlated field using Theo's values
    X, Y = np.meshgrid(np.arange(-30, 30), np.arange(-30, 30)) # U
    filter_kernel = np.exp(-(X*X + Y*Y)/(2*30))
    noise = 1.11 * np.random.randn(U.shape[0], U.shape[1])
    corrNoiseU = scipy.signal.convolve(noise, filter_kernel, mode = 'same')
    corrNoiseU *= (noise.std() / corrNoiseU.std())
    U += corrNoiseU

    X, Y = np.meshgrid(np.arange(-20, 20), np.arange(-20, 20)) # U
    filter_kernel = np.exp(-(X*X + Y*Y)/(2*20))
    noise = 1.11 * np.random.randn(V.shape[0], V.shape[1])
    corrNoiseV = scipy.signal.convolve(noise, filter_kernel, mode = 'same')
    corrNoiseV *= (noise.std() / corrNoiseV.std())
    V += corrNoiseV

    # Sometimes cloud cover is slightly negative ..
    CLCT[CLCT < 0] = 0

    # Using https://www.sandhurstweather.org.uk/barometric.pdf to convert mean sea level
    # pressure to pressure at the lake's height.
    P = PMSL / np.exp(gridP.GenevaRasterHeightASL / (29.263*Temp))

    # Convert wind to surface stress in accordance with `Small scale hydrodynamics in
    # lakes` by Wuest and Lorke.
    if not forRS:
        rho_air = P / (287.058 * Temp)
        C10 = np.zeros(U.shape)
        windSpeedSqrd = U**2 + V**2
        for (i, j), _ in np.ndenumerate(U):
            C10[i, j] = computeC10(windSpeedSqrd[i, j])

        Ustress = rho_air * C10 * np.sqrt(windSpeedSqrd) * U
        Vstress = rho_air * C10 * np.sqrt(windSpeedSqrd) * V

    # Compute specific humidity from the relative value; based on
    # formula from https://earthscience.stackexchange.com/a/2361
    vaporPressure = 6.11 * np.exp(17.67 * (Temp-273.15) / (Temp-29.65)) # in units of hPa
    SH = (HUM*vaporPressure) / (0.263*6.11*P)

    # Estimate downward radiation based on Alfred Wüest's course Limnology, chapter 5.
    A_L = 0.03   # Infrared radiation albedo
    a = 1.09     # Calibration parameter
    E_a = a * (1 + 0.17 * np.power(CLCT, 2)) * 1.24 * np.power(vaporPressure / Temp, 1./7)
    LWR = (1 - A_L) * 5.67e-8 * E_a * np.power(Temp, 4)

    if forRS:
        return Temp, SWR, LWR, HUM, CLCT, U, V
    else:
        return Temp, P, SWR, LWR, SH, Ustress, Vstress


def extractData2(currentDate, fileNames, MSindex):
    # Generate a reproducible random seed
    seed = int(str(currentDate.date()).replace('-', '')) * MSindex
    np.random.seed(seed)

    MSfile = f'{FilePaths.MeteoSwissDataPath}/E2/' \
             f'cosmoE_epfl_lakes_{currentDate.strftime("%Y%m%d")}.nc'
    assert os.path.isfile(MSfile), \
        f'Error: required Meteoswiss file <{MSfile}> not found'
    MSdata = Dataset(MSfile, mode = 'r')

    # Extract support points for the MeteoSwiss data on the computational grid.
    # Also extract the grid points with respect to the center of each cell.
    longitude = MSdata['lon_1'][50:80, 55:100]          # x-coord
    latitude = MSdata['lat_1'][50:80, 55:100]           # y-coord

    sourcePts, destPts, relAngle = getSupportPoints(latitude, longitude)

    # Generate destination points for the U and V coordinates
    destPtsU = destPts.copy()
    destPtsV = destPts.copy()
    destPtsU[:, 0] -= meshSize/2
    destPtsV[:, 1] -= meshSize/2
    ptsList = [destPts, destPts, destPts, destPts, destPts, destPtsU, destPtsV]

    initH = 1 if currentDate > startTime else 0
    for hour in range(initH, 25):
        Temp, P, SWR, LWR, SH, Su, Sv = extractData2_aux(hour, MSdata, seed)

        # Rotate Meteoswiss data to align with the computational grid.
        SuRotated = np.cos(relAngle)*Su - np.sin(relAngle)*Sv
        SVRotated = np.sin(relAngle)*Su + np.cos(relAngle)*Sv

        # Convert the entire netCDF dataset
        varList = [Temp, P, SWR, LWR, SH, SuRotated, SVRotated]
        for i in range(len(varList)):
            output = open(fileNames[i], 'ab')

            interpolator = RegularGridInterpolator(sourcePts, varList[i])
            data = interpolator(ptsList[i])
            data.tofile(output)
            output.close()

    MSdata.close()


def extractData3_aux(hour, MSdata, MSindex, forRS = False):
    Temp = MSdata['T_2M'][hour, MSindex, :]       # Temperature, in K
    U = MSdata['U_10M'][hour, MSindex, :]         # Eastward wind
    V = MSdata['V_10M'][hour, MSindex, :]         # Northward wind
    PMSL = MSdata['PMSL'][hour, MSindex, :]       # Pressure at sea level
    HUM = MSdata['RELHUM_2M'][hour, MSindex, :]   # Relative humidity index
    CLCT = MSdata['CLCT'][hour, MSindex, :]/100 # Fractional cloud cover
    SWR = MSdata['GLOB'][hour, MSindex, :]      # Short-wave radiation

    # Sometimes cloud cover is slightly negative ..
    CLCT[CLCT < 0] = 0

    # Using https://www.sandhurstweather.org.uk/barometric.pdf to convert mean sea level
    # pressure to pressure at the lake's height.
    P = PMSL / np.exp(gridP.GenevaRasterHeightASL / (29.263*Temp))

    # Convert wind to surface stress in accordance with `Small scale hydrodynamics in
    # lakes` by Wuest and Lorke.
    if not forRS:
        rho_air = P / (287.058 * Temp)
        C10 = np.zeros(U.shape)
        windSpeedSqrd = U**2 + V**2
        for (i, j), _ in np.ndenumerate(U):
            C10[i, j] = computeC10(windSpeedSqrd[i, j])

        Ustress = rho_air * C10 * np.sqrt(windSpeedSqrd) * U
        Vstress = rho_air * C10 * np.sqrt(windSpeedSqrd) * V

    # Compute specific humidity from the relative value; based on
    # formula from https://earthscience.stackexchange.com/a/2361
    vaporPressure = 6.11 * np.exp(17.67 * (Temp-273.15) / (Temp-29.65)) # in units of hPa
    SH = (HUM*vaporPressure) / (0.263*6.11*P)

    # Estimate downward radiation based on Alfred Wüest's course Limnology, chapter 5.
    A_L = 0.03   # Infrared radiation albedo
    a = 1.09     # Calibration parameter
    E_a = a * (1 + 0.17 * np.power(CLCT, 2)) * 1.24 * np.power(vaporPressure / Temp, 1./7)
    LWR = (1 - A_L) * 5.67e-8 * E_a * np.power(Temp, 4)

    if forRS:
        return Temp, SWR, LWR, HUM, CLCT, U, V
    else:
        return Temp, P, SWR, LWR, SH, Ustress, Vstress


def extractData3(currentDate, fileNames, MSindex):
    MSfile = f'{FilePaths.MeteoSwissDataPath}/E3/' \
        f'VNJK21.{(currentDate - Timedelta(days = 1)).strftime("%Y%m%d")}0000.nc'
    assert os.path.isfile(MSfile), \
        f'Error: required Meteoswiss file <{MSfile}> not found'
    MSdata = Dataset(MSfile, mode = 'r')
    # Extract support points for the MeteoSwiss data on the computational grid.
    # Also extract the grid points with respect to the center of each cell.
    longitude = MSdata['lon_1'][:]          # x-coord
    latitude = MSdata['lat_1'][:]           # y-coord

    sourcePts, destPts, relAngle = getSupportPoints(latitude, longitude)

    # Generate destination points for the U and V coordinates
    destPtsU = destPts.copy()
    destPtsV = destPts.copy()
    destPtsU[:, 0] -= meshSize/2
    destPtsV[:, 1] -= meshSize/2
    ptsList = [destPts, destPts, destPts, destPts, destPts, destPtsU, destPtsV]

    for hour in range(0, 24):
        Temp, P, SWR, LWR, SH, Su, Sv = extractData3_aux(hour, MSdata, MSindex)

        # Rotate Meteoswiss data to align with the computational grid.
        SuRotated = np.cos(relAngle)*Su - np.sin(relAngle)*Sv
        SVRotated = np.sin(relAngle)*Su + np.cos(relAngle)*Sv

        # Convert the entire netCDF dataset
        varList = [Temp, P, SWR, LWR, SH, SuRotated, SVRotated]
        for i in range(len(varList)):
            output = open(fileNames[i], 'ab')

            interpolator = RegularGridInterpolator(sourcePts, varList[i],
                                                   bounds_error = False,
                                                   fill_value = None)
            data = interpolator(ptsList[i])
            data.tofile(output)
            output.close()
    MSdata.close()

    if currentDate + Timedelta(days = 1) == endTime:
        MSfile = f'{FilePaths.MeteoSwissDataPath}/E3/' \
            f'VNJK21.{currentDate.strftime("%Y%m%d")}0000.nc'
        assert os.path.isfile(MSfile), \
            f'Error: required Meteoswiss file <{MSfile}> not found'
        MSdata = Dataset(MSfile, mode = 'r')

        Temp, P, SWR, LWR, SH, Su, Sv = extractData3_aux(0, MSdata, MSindex)

        # Rotate Meteoswiss data to align with the computational grid.
        SuRotated = np.cos(relAngle)*Su - np.sin(relAngle)*Sv
        SVRotated = np.sin(relAngle)*Su + np.cos(relAngle)*Sv

        # Convert the entire netCDF dataset
        varList = [Temp, P, SWR, LWR, SH, SuRotated, SVRotated]
        for i in range(len(varList)):
            output = open(fileNames[i], 'ab')

            interpolator = RegularGridInterpolator(sourcePts, varList[i],
                                                   bounds_error = False,
                                                   fill_value = None)
            data = interpolator(ptsList[i])
            data.tofile(output)
            output.close()

        MSdata.close()


def interpolate(outputFolder, MSindex):
    '''
    Interpolates one of the ensembles (0 <= MSindex <= 20) from the MeteoSwiss weather
    predictions onto the computational grid for a time interval ranging from `startDate`
    and `endDate`. The only assumption here is that 'startTime' is at midnight.
    '''

    assert startTime.hour == 0 and startTime.minute == 0
    currentDate = startTime

    # List of output filenames. Since we write in append mode, we also ensure
    # that these files do not exist already.
    fileNames = [f'{outputFolder}/E_{MSindex}/EXF_T_{meshSize}m.bin',
                 f'{outputFolder}/E_{MSindex}/EXF_P_{meshSize}m.bin',
                 f'{outputFolder}/E_{MSindex}/EXF_SWR_{meshSize}m.bin',
                 f'{outputFolder}/E_{MSindex}/EXF_LWR_{meshSize}m.bin',
                 f'{outputFolder}/E_{MSindex}/EXF_HUM_{meshSize}m.bin',
                 f'{outputFolder}/E_{MSindex}/EXF_Ustress_{meshSize}m.bin',
                 f'{outputFolder}/E_{MSindex}/EXF_Vstress_{meshSize}m.bin']

    for fileN in fileNames:
        if os.path.isfile(fileN):
            os.remove(fileN)

    # Coverage of Meteoswiss data by different formats
    MS1begin = datetime(year = 2017, month = 1, day = 1, tzinfo = timezone.utc)
    MS1end = datetime(year = 2020, month = 4, day = 16, tzinfo = timezone.utc)
    MS2begin = datetime(year = 2020, month = 4, day = 16, tzinfo = timezone.utc)
    MS2end = datetime(year = 2021, month = 5, day = 25, tzinfo = timezone.utc)
    MS3begin = datetime(year = 2021, month = 5, day = 25, tzinfo = timezone.utc)

    # Now we interpolate the MeteoSwiss data.
    while currentDate < endTime:
        if MS1begin <= currentDate < MS1end:
            extractData1(currentDate, fileNames, MSindex)
            currentDate += Timedelta(hours = 12)
        elif MS2begin <= currentDate < MS2end:
            extractData2(currentDate, fileNames, MSindex)
            currentDate += Timedelta(hours = 24)
        elif MS3begin <= currentDate < datetime.now(tz = timezone.utc):
            extractData3(currentDate, fileNames, MSindex)
            currentDate += Timedelta(hours = 24)


def updateInputFileEXF(currentT, runPath):
    '''
    Updates MITgcm configuration file `data.exf` to have the correct starting day for the
    surface forcing terms. This function is primarily for the sequential update routine.
    '''

    startingPoint = startTime + Timedelta(seconds = currentT)
    startingDay = startingPoint.strftime('%Y%m%d')

    modifyDataEXF = f'{runPath}/data.exf'
    with open(modifyDataEXF, 'r') as fileN:
        dataIn = fileN.readlines()

    with open(modifyDataEXF, 'w') as dataOut:
        for line in dataIn:
            if 'startdate1' in line:
                loc = line.find('=')
                dataOut.write(f'{line[0:loc]}= {startingDay}\n')
            else:
                dataOut.write(line)


def updateInputPaths(baseDir, outputFolder, MSindex):
    '''
    As we pre-interpolate the MeteoSwiss weather ensembles (in the SPUX module),
    at run-time we only need to paths to point to the right directory, which is
    what this function does.
    '''

    prefix = f'{baseDir}/E_{MSindex}/EXF_'.replace('//', '/')
    varMap = {'ustressfile'   : f'"{prefix}Ustress_{meshSize}m.bin"',
              'vstressfile'   : f'"{prefix}Vstress_{meshSize}m.bin"',
              'atempfile'     : f'"{prefix}T_{meshSize}m.bin"',
              'swdownfile'    : f'"{prefix}SWR_{meshSize}m.bin"',
              'lwdownfile'    : f'"{prefix}LWR_{meshSize}m.bin"',
              'aqhfile'       : f'"{prefix}HUM_{meshSize}m.bin"',
              'apressurefile' : f'"{prefix}P_{meshSize}m.bin"'}

    modifyDataEXF = f'{outputFolder}/data.exf'
    modifyArguments(varMap, modifyDataEXF, modifyDataEXF)
