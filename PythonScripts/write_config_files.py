'''
This script writes the run-time parameters for the simulation (specifically, the data*
files) by using the settings stored over at 'global_parameters.py'.
'''

import numpy as np
import auxiliary_functions as aux

from config_settings import startTime, meshSize, withMPI, withRivers, initialCond
from PythonScripts.global_parameters import (GridParams as gridP,
                                             HydrodynamicParameters as hydroP,
                                             Discretization)


assert meshSize in Discretization.keys(), (
    'ERROR: settings not implemented for this mesh size. Either change to a supported '
    'mesh size or supply the correct parameters for this setting in '
    '<GenerateInputData/global_parameters.py>')
meshP = Discretization[meshSize]

# First, do some consistency checks:
assert (meshP.sNx * meshP.nSx * meshP.nPx == meshP.minX and
        meshP.sNy * meshP.nSy * meshP.nPy == meshP.minY), (
    'ERROR: inconsistent parallelization options: product of partitions is not equal to '
    'the global dimension')
if not withMPI:
    assert meshP.nSx * meshP.nPx == 1 and meshP.nSy * meshP.nPy == 1, \
        'ERROR: cannot have parallel partitioning for a serial code'

# Now interpolate for temperature based on the vertical discretization. Here we assume
# that the data comes from the Simstrat model, where the grid is uniformly 1m in size.
# Note: value 0 corresponds to the top of the lake.
domSource = range(0, gridP.GenevaMaxDepth + 1)
domModel = [sum(meshP.verticalSpacing[0:i]) for i in range(meshP.verticalN)]
initTemperature = np.interp(domModel, domSource, hydroP.temperatureSimstrat)
initSalinity = np.full((meshP.verticalN), hydroP.salinityValue)

###################### Global hydrodynamic options ############################
templatePKG = 'Hydrodynamics/template/data.pkg'
generatedPKG = 'Hydrodynamics/run/data.pkg'
paramsPKG = {'useOBCS' : '.TRUE.' if withRivers else '.FALSE.',
             'useEXF'  : '.TRUE.'}
aux.modifyArguments(paramsPKG, templatePKG, generatedPKG)

###################### Simulation start time ##################################
templateCAL = 'Hydrodynamics/template/data.cal'
outCAL = 'Hydrodynamics/run/data.cal'
paramsCAL = {'startDate_1' : startTime.strftime("%Y%m%d")}
aux.modifyArguments(paramsCAL, templateCAL, outCAL)

###################### Meteorological forcing #################################
templateEXF = 'Hydrodynamics/template/data.exf'
outEXF = 'Hydrodynamics/run/data.exf'
paramsCAL = {'exf_albedo': hydroP.albedo_value, 'cdalton': hydroP.DaltonCoeff}
aux.modifyArguments(paramsCAL, templateEXF, outEXF)

fieldsEXF = ['!setMeshSize!', '!setStartDate!']
valuesEXF = [str(meshSize), startTime.strftime("%Y%m%d")]
aux.searchAndReplace(fieldsEXF, valuesEXF, outEXF)

###################### River data ############################################
fileOBCS = 'Hydrodynamics/run/data.obcs'

riverString = ''
for index in meshP.RiverIndices.values():
    if index[0] == 'Iwest' or index[0] == 'Ieast':
        riverString += f' OB_{index[0]}({index[2]}) = {index[1]}\n'
    elif index[0] == 'Jsouth':
        riverString += f' OB_Jsouth({index[1]}) = {index[2]}\n'

aux.searchAndReplace([' !setOBs!'], [riverString[:-1]], fileOBCS)

###################### Parallelization options ################################
templateSIZEh = 'Hydrodynamics/template/SIZE.h'
outSIZEh = 'Hydrodynamics/code/SIZE.h'
paramsSIZEh = {'Nx'  : f'{meshP.nPx * meshP.nSx * meshP.sNx}',
               'Ny'  : f'{meshP.nPy * meshP.nSy * meshP.sNy}',
               'sNx' : f'{meshP.sNx}',
               'sNy' : f'{meshP.sNy}',
               'nSx' : f'{meshP.nSx}',
               'nSy' : f'{meshP.nSy}',
               'nPx' : f'{meshP.nPx}',
               'nPy' : f'{meshP.nPy}',
               'Nr'  : f'{meshP.verticalN}'}
aux.modifyArguments(paramsSIZEh, templateSIZEh, outSIZEh)

###################### Physical model parameters ##############################
templateDATA = 'Hydrodynamics/template/data'
outDATA = 'Hydrodynamics/run/data'
fieldsDATA = {'tRef' : aux.printArray(initTemperature),
              'sRef' : aux.printArray(initSalinity),
              'diffKzT' : hydroP.vertDiffusivity,
              'diffKhT' : meshP.HorizDiffusivity,
              'bottomDragQuadratic' : hydroP.bottomDrag,
              'viscAz' : hydroP.vertViscosity,
              'viscAhGrid' : meshP.HorizViscosity,
              'abEps' : hydroP.adamsBashforthStabilizingTerm,
              'deltaT' : meshP.deltaT,
              'viscC2smag' : meshP.smagorinskyCoeff,
              'dumpFreq' : hydroP.outputFrequency,
              'monitorFreq' : hydroP.monitorFrequency,
              'dYspacing' : meshSize,
              'dXspacing' : meshSize,
              'delZ' : aux.printArray(meshP.verticalSpacing),
              'bathyFile' : f'"../binary_data/bathyGeneva_{meshSize}m.bin"'}
if initialCond:
    fieldsDATA['pickupSuff'] = '"0000000000"'
aux.modifyArguments(fieldsDATA, templateDATA, outDATA)
