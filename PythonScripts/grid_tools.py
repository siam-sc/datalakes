'''
A class that handles all grid-related tasks such as conversions between points
on the computational grid and Swiss grid coordinates.
'''

import os
import glob
import numpy as np
from re import search
from netCDF4 import Dataset  # pylint: disable=no-name-in-module
import pickle

from config_settings import meshSize
from PythonScripts.global_parameters import GridParams as gridP, Discretization
meshP = Discretization[meshSize]


class GridTools:
    gridFetched = False
    vertDiscretization = None
    gridAngle = np.arctan2(gridP.Y1-gridP.Y0, gridP.X1-gridP.X0)


    def __init__(self, gridPath = None):
        self.partitioning = [meshP.nPx*meshP.nSx, meshP.nPy*meshP.nSy]
        self.bathymetry = np.zeros([meshP.minX, meshP.minY])
        self.tileMapping = np.zeros(self.partitioning, dtype = 'U3')
        if gridPath is not None:
            self.init(gridPath)


    def init(self, gridPath):
        '''
        Here we initialize the pattern for the parallel partitioning - in
        particular, where a tile xxx is located on the 2-dimensional grid.
        We assume that this discretization is deterministic for each
        configuration, and thus do this only once, after the netcdf grid files
        are generated.
        '''

        nTiles = np.prod(self.partitioning)
        gridFileNames = glob.glob(f'{gridPath}/grid.*.nc')
        assert len(gridFileNames) == nTiles, \
            'Error: unexpected tile count for netcdf grid files'

        self.vertDiscretization = Dataset(gridFileNames[0])['Z'][:]

        # Extract data from individual tiles.
        for fileName in gridFileNames:
            gridFile = Dataset(fileName)
            gridFile.set_always_mask(False)

            tileBathymetry = np.array(gridFile['Depth']).transpose()

            indX = int(gridFile['Xp1'][0]/(meshSize*meshP.sNx))
            indY = int(gridFile['Yp1'][0]/(meshSize*meshP.sNy))

            # Make sure the dimensions are consistent, and therefore the int()
            # function did not actually perform any rounding.
            assert(indX*meshSize*meshP.sNx == gridFile['Xp1'][0] and
                   indY*meshSize*meshP.sNy == gridFile['Yp1'][0]), \
                'Error: unexpected tile partitioning'

            self.tileMapping[indX, indY] = search('\\.t(.*)\\.', fileName)[1]
            self.bathymetry[indX*meshP.sNx:(indX+1)*meshP.sNx,
                            indY*meshP.sNy:(indY+1)*meshP.sNy] = tileBathymetry

        # Check that all tiles have been initialized by computing their sum.
        sum = 0
        for tile in np.ndenumerate(self.tileMapping):
            sum += int(tile[1])
        assert sum == nTiles*(nTiles+1)/2, \
            'Error: tile map has not been properly initialized'

        self.gridFetched = True


    def cellContainsWater(self, indX, indY, indZ):
        '''
        Warning: this function may return false for a partially filled
        ('shaved') cell. A more robust option would be to import some solution
        file and extract the indices for non-zero values for temperature.
        '''

        return self.bathymetry[indX, indY] >= -self.vertDiscretization[indZ]


    def SwissGridToCompGrid(self, xSG, ySG):
        '''
        Convert a Swiss coordinate system (xSG, ySG) point to a corresponding
        point on the computational grid.
        '''

        xP = xSG - gridP.X0
        yP = ySG - gridP.Y0
        x = np.cos(self.gridAngle)*xP + np.sin(self.gridAngle)*yP
        y = -np.sin(self.gridAngle)*xP + np.cos(self.gridAngle)*yP
        return (x, y)


    def CompGridToSwissGrid(self, x, y):
        '''
        Converts a point (x, y) on the computational grid to a point in the
        Swiss coordinate system.
        '''

        xSG = (np.cos(self.gridAngle)*x - np.sin(self.gridAngle)*y) + gridP.X0
        ySG = (np.sin(self.gridAngle)*x + np.cos(self.gridAngle)*y) + gridP.Y0
        return (xSG, ySG)


    def PointToIndex(self, x, y):
        '''
        From a physical point on the surface of the computational grid finds
        the tile and the cell containing the point, with the index of the
        cell being relative to the tile.
        '''

        indX = int(x/meshSize)
        indY = int(y/meshSize)
        tileIndX = int(indX/meshP.sNx)
        tileIndY = int(indY/meshP.sNy)

        assert (self.gridFetched and
                tileIndX < self.partitioning[0] and indX < meshP.minX and
                tileIndY < self.partitioning[1] and indY < meshP.minY), \
            'Error: specified point is somehow invalid.'

        TileX = indX - tileIndX*meshP.sNx
        TileY = indY - tileIndY*meshP.sNy
        return (TileX, TileY, self.tileMapping[tileIndX, tileIndY])


    def extractSurfaceSolution(self, runPath):
        '''
        Uses the computational grid pattern to extract the surface solution.
        '''

        surfaceT = np.zeros([meshP.minX, meshP.minY])

        for (i, j), tileName in np.ndenumerate(self.tileMapping):
            simFileName = f'{runPath}/pickup.ckptA.t{tileName}.nc'
            assert os.path.isfile(simFileName)
            tileSoln = Dataset(simFileName)

            surfaceT[i*meshP.sNx:(i+1)*meshP.sNx,
                     j*meshP.sNy:(j+1)*meshP.sNy] = tileSoln['Temp'][0, 0, :, :].T
        return surfaceT


    def save(self, saveFile):
        with open(saveFile, 'wb') as dumpfile:
            pickle.dump(self, dumpfile)


    def load(self, saveFile):
        # Note: will automatically switch `gridFetched` to True.
        if os.path.isfile(saveFile):
            with open(saveFile, 'rb') as dataFile:
                dataFile = pickle.load(dataFile)
                self.__dict__.update(dataFile.__dict__)


def LatLonToSwissGrid(latitude, longitude):
    '''
    Convert a (longitude, latitude) point to the Swiss coordinate system,
    in accordance with the formulas given in 'Approximate formulas for the
    transformation between Swiss projection coordinates and WGS84'.
    '''

    phiPrime = (3600*latitude - 169028.66) / 10000
    lamPrime = (3600*longitude - 26782.5) / 10000
    xCoord = ( 600072.37 +
               211455.93 * lamPrime -
               10938.51  * lamPrime*phiPrime -
               0.36      * lamPrime*phiPrime**2 -
               44.54     * lamPrime**3 )
    yCoord = ( 200147.07 +
               308807.95 * phiPrime +
               3745.25   * lamPrime**2 +
               76.63     * phiPrime**2 -
               194.56    * phiPrime*lamPrime**2 +
               119.79    * phiPrime**3 )

    return (xCoord, yCoord)


def SwissGridToLatLon(xSG, ySG):
    '''
    Convert a Swiss coordinate system (xSG, ySG) point to the geographic
    coordinate system in accordance with the formulas given in 'Approximate
    formulas for the transformation between Swiss projection coordinates
    and WGS84'.
    '''

    yPrime = (xSG - 600000) / 1000000
    xPrime = (ySG - 200000) / 1000000

    longitudePrime = ( 2.6779094 +
                       4.728982  * yPrime +
                       0.791484  * xPrime*yPrime +
                       0.1306    * yPrime*xPrime**2 -
                       0.0436    * yPrime**3 )
    latitudePrime = ( 16.9023892 +
                      3.238272   * xPrime -
                      0.270978   * yPrime**2 -
                      0.002528   * xPrime**2 -
                      0.0447     * xPrime*yPrime**2 -
                      0.0140     * xPrime**3 )

    longitude = (100./36.) * longitudePrime
    latitude  = (100./36.) * latitudePrime

    return (latitude, longitude)
