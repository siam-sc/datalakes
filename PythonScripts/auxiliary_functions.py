'''
Helper functions
'''

import os, glob, shutil, sys
from contextlib import contextmanager

# Color output
textGreen = '\033[92m'
textRed   = '\033[91m'
textReset = '\033[0m'
printGreen = lambda text: print(f'{textGreen}{text}{textReset}')
printRed   = lambda text: print(f'{textRed}{text}{textReset}')


def modifyArguments(params, fileIn, fileOut):
    '''
    Function to modify run-time parameters, based on variable name, with the
    assumption that they are stored the file as '!varName!'
      params  - a dictionary of variable names and corresponding replacements
      fileIn  - template configuration
      fileOut - output run-time configuration
    '''

    with open(fileIn, 'r') as infile:
        lines = infile.readlines()

    for varName in params:
        valueModified = False
        for index, line in enumerate(lines):
            if varName in line and '=' in line:
                lines[index] = f'{line[0:line.find("=")]}= {params[varName]}\n'
                valueModified = True

        if not valueModified:
            print(f'Warning: variable <{varName}> not found in datafiles!')

    with open(fileOut, 'w') as outfile:
        for line in lines:
            outfile.write(line)


def updateInputFiles(currentT, endTime, currentIter, inputFile, setPickup = True):
    fields = {'startTime'   : currentT,
              'endTime'     : endTime,
              'nIter0'      : currentIter}
    if setPickup or currentIter > 0:
        fields['pickupSuff'] = f'"{currentIter:010d}"'
    else:
        fields['pickupSuff'] = '" "'
    modifyArguments(fields, inputFile, inputFile)


def searchAndReplace(searchList, replaceList, fileName):
    '''
    Function to search and replace strings within a file.
    '''

    with open(fileName, 'r') as file:
        contents = file.read()

    for i in range(len(searchList)):
        if not searchList[i] in contents:
            print(f'Warning: <{searchList[i]}> not found in {fileName}!')
        contents = contents.replace(searchList[i], replaceList[i])

    with open(fileName, 'w') as file:
        file.write(contents)


def extractVariable(searchStr, fileName):
    with open(fileName, 'r') as file:
        lines = file.readlines()

    for line in lines:
        if searchStr in line:
            loc = line.find('=')+1
            return line[loc:-1].strip()

    # If we reach this point, then the string has not been found.
    raise Exception(f'Error: {searchStr} not found in {fileName}!')


def printArray(array):
    '''
    Function to print an array to a multiline string, with 10 numbers per line.
    '''

    arraySize = len(array)
    i = 0
    outputString = '\n    '

    while (i < arraySize):
        count = min(10, arraySize-i)
        outputString += ', '.join(f'{x:2.3f}' for x in array[i:(i+count)])
        outputString += ',\n    '
        i += 10

    return outputString


def removeFiles(inputList, prefix):
    for expr in inputList:
        for fName in glob.glob(f'{prefix}/{expr}'):
            try:
                os.remove(fName)
            except OSError:
                print(f'Unable to remove : {fName}')


def moveFiles(inputList, prefix, destination):
    for expr in inputList:
        for fName in glob.glob(f'{prefix}/{expr}'):
            try:
                shutil.move(fName, f'{prefix}/{destination}')
            except OSError:
                print(f'Unable to move : {fName}')


# Based on https://stackoverflow.com/a/17954769
@contextmanager
def redirectSTDOUT(to = os.devnull):
    fd = sys.stdout.fileno()

    def _redirect_stdout(to):
        sys.stdout.close()
        os.dup2(to.fileno(), fd)
        sys.stdout = os.fdopen(fd, 'w')

    with os.fdopen(os.dup(fd), 'w') as old_stdout:
        with open(to, 'w') as file:
            _redirect_stdout(to = file)
        try:
            yield
        finally:
            _redirect_stdout(to = old_stdout)
