import glob
from pandas import Timedelta, read_csv
from netCDF4 import Dataset  # pylint: disable=no-name-in-module
from scipy.signal import savgol_filter

from config_settings import FilePaths, meshSize, startTime as initDay
from PythonScripts.global_parameters import GridParams


class ManageWaterLevels:
    '''
    Scripts for reading in BAFU water levels and adjusting simulated water
    level heights to measured data from station 2028.
    '''


    def __init__(self, verbosity = 0):
        self.verbosity = verbosity


    def init(self, gridTools):
        '''
        The indices closest to the water station and the corresponding file
        will depend on the mesh size and parallel partitioning scheme. These
        parameters do not change after the simulation starts however, so we
        can pre-compute those quantities after the first run using the
        'GridTools' class. Since the point may end up not being on a water
        cell, we gradually move it eastward until a water cell is found.
          verbosity - supply 1 to print the relative change after each
                      adjustment
        '''

        (SGx, SGy) = (500750, 119390)       # Swiss grid station coordinates

        for i in range(20):
            (x, y) = gridTools.SwissGridToCompGrid(SGx + 50*i, SGy)
            (self.indX, self.indY, tile) = gridTools.PointToIndex(x, y)

            indX = int(x/meshSize)
            indY = int(y/meshSize)
            if gridTools.bathymetry[indX, indY] > 0:
                break

        # Make sure that this is a water cell.
        assert gridTools.bathymetry[indX, indY] > 0, \
            'Error: invalid cell - no water in it'

        # Name of the pickupfile that will contain the station.
        self.pickupFile = f'pickup.ckptA.t{tile}.nc'


    def extractWaterLevel(self, currentT):
        '''
        Extracts an hourly time-series of the riverine data for the requested day.
          currentT - time in seconds from initDay
        '''

        day = initDay + Timedelta(seconds = currentT)

        heights = read_csv(f'{FilePaths.BafuDataPath}/station_2028/Year_{day.year}.csv',
                           index_col = 'Date_Time', parse_dates = True)
        heights = heights[heights.index.date == day.date()]

        # Smooth the dataset as it has a sinusoidal noise.
        heights['Level'] = savgol_filter(heights['Level'],
                                         window_length = 31,
                                         polyorder = 3, mode = 'nearest')

        return heights - GridParams.GenevaRasterHeightASL


    def adjustWaterLevel(self, sandboxPath, currentT, heights):
        '''
        Adjusts the netcdf pickup files to have the correct water levels.
        Inputs:
          sandboxPath  - path to the parent directory of the 'run' folder
          startTime    - elapsed time (sec) from the start of the simulation
          hourlyLevels - hourly measured water levels for the particular day
        '''

        day = initDay + Timedelta(seconds = currentT)
        closestRecord = heights.index.get_loc(day, method = 'nearest')
        level = heights.iloc[closestRecord]['Level']

        # Read in water level from the relevant file, and compute the height differential.
        # In some cases, the pickup file will not be generated instantly, and therefore we
        # need to wait.
        pickupFileName = f'{sandboxPath}/{self.pickupFile}'
        stationFile = Dataset(pickupFileName)

        TileWaterLevel = stationFile['Eta'][0, self.indX, self.indY]
        heightDifferential = level - TileWaterLevel
        if self.verbosity:
            print(f'    Height adjustment: {heightDifferential:g}')

        # Update pickup file levels
        pickupNames = glob.glob(f'{sandboxPath}/pickup.ckptA.*.nc')
        for index in range(len(pickupNames)):
            pickupFile = Dataset(pickupNames[index], 'r+')
            pickupFile['Eta'][:] += heightDifferential
            pickupFile.close()
