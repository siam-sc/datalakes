from dataclasses import dataclass
import numpy as np


# Horizontal diffusivity/viscosity values, based on Lawrence et al, and conversion
# formula : viscAhGrid = 4 * deltaT * viscAh / cellSize^2
Kh = lambda cellSize : 3.2e-4 * pow(cellSize, 1.1)
horizDiffViscosity = lambda cellSize, deltaT : (1.28e-3 * deltaT) / pow(cellSize, .9)


@dataclass
class GridParams:
    # Southmost and westmost points of the initialization routine.
    (X0, Y0) = (500000, 116500)
    (X1, Y1) = (563000, 138700)
    distY = 25. * 1000

    # Depth of the river at the junction with the lake (estimated)
    depth2009 = 2       # Porte du Scex, coordinates: (557660, 133280)
    depth2606 = 3       # Geneva, coordinates: (499890, 117850)

    # Mesh parameters from Rosi's rasterization
    GenevaRasterLeftX = 500391.6063
    GenevaRasterLeftY = 117792.7875
    GenevaRasterCellSize = 10
    GenevaRasterHeightASL = 372
    GenevaMaxDepth = 310


@dataclass
class options1000:
    deltaT = 60
    minX = 70
    minY = 26
    verticalSpacing = [1, 1, 1, 1, 1, 1.1, 1.2, 1.3, 1.4, 1.5,
                       1.6, 1.7, 1.8, 1.9, 2.0, 2.1, 2.2, 2.3, 2.4, 2.5,
                       2.6, 2.7, 2.8, 2.9, 3, 3.2, 3.5, 4, 4.5, 5,
                       5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9, 10, 11,
                       12, 13, 14, 15, 16, 17, 18, 19, 20, 21]
    verticalN = len(verticalSpacing)
    sNx = 70
    sNy = 26
    nSx = 1
    nSy = 1
    nPx = 1
    nPy = 1
    RiverIndices = {0: ('Iwest', 3, 3),
                    1: ('Iwest', 3, 4),
                    3: ('Jsouth', 63, 2)}

    HorizViscosity = 0.6e-3
    HorizDiffusivity = 0.5
    smagorinskyCoeff = 2.0    # Smagorinsky coefficient for horizontal viscosity

@dataclass
class options450:
    deltaT = 20
    minX = 150
    minY = 56
    verticalSpacing = [0.33, 0.36, 0.39, 0.42, 0.45, 0.48, 0.51, 0.54, 0.57, 0.60,
                       0.63, 0.66, 0.69, 0.72, 0.75, 0.78, 0.81, 0.84, 0.87, 0.90,
                       0.93, 0.96, 0.99, 1.02, 1.05, 1.08, 1.11, 1.14, 1.17, 1.20,
                       1.23, 1.26, 1.29, 1.32, 1.35, 1.38, 1.41, 1.44, 1.47, 1.50,
                       1.53, 1.56, 1.59, 1.62, 1.65, 1.68, 1.71, 1.74, 1.77, 1.80,
                       1.83, 1.86, 1.89, 2.01, 2.13, 2.25, 2.37, 2.49, 2.61, 2.73,
                       2.85, 2.97, 3.09, 3.21, 3.33, 3.45, 3.57, 3.69, 3.81, 3.93,
                       4.05, 4.17, 4.29, 4.41, 4.53, 4.65, 4.77, 4.89, 5.01, 5.13,
                       5.25, 5.37, 5.49, 5.61, 5.73, 5.85, 5.97, 6.37, 6.77, 7.17,
                       7.57, 7.97, 8.37, 8.77, 9.17, 9.57, 9.97, 10.37, 10.77, 11.17]
    sNx = 150
    sNy = 56
    nSx = 1
    nSy = 1
    nPx = 1
    nPy = 1
    RiverIndices = {0: ('Iwest', 5, 5),
                    1: ('Jsouth', 5, 5),
                    2: ('Jsouth', 131, 5),
                    3: ('Jsouth', 132, 5)}

@dataclass
class options200:
    deltaT = 6
    minX = 336
    minY = 132
    verticalSpacing = np.concatenate([np.linspace(0.15, 0.3, 16),
                                      np.linspace(0.3, .5, 41),
                                      np.linspace(0.5, .8, 31),
                                      np.linspace(0.8, 1.0, 11),
                                      np.linspace(1.0, 8, 41),
                                      [8]*10])
    verticalN = len(verticalSpacing)
    sNx = 28
    sNy = 44
    nSx = 1
    nSy = 1
    nPx = 12
    nPy = 3
    RiverIndices = {0: ('Iwest', 9, 10),
                    1: ('Jsouth', 9, 10),
                    2: ('Ieast', 298, 10),
                    3: ('Jsouth', 298, 10),
                    4: ('Ieast', 298, 9)}
    smagorinskyCoeff = 0.3    # Smagorinsky coefficient for horizontal viscosity
    HorizDiffusivity = Kh(200)
    HorizViscosity = horizDiffViscosity(200, deltaT)

@dataclass
class options100:
    deltaT = 6
    minX = 672
    minY = 264
    verticalSpacing = np.concatenate([np.linspace(0.15, 0.3, 16),
                                      np.linspace(0.3, .5, 41),
                                      np.linspace(0.5, .8, 31),
                                      np.linspace(0.8, 1.0, 11),
                                      np.linspace(1.0, 8, 41),
                                      [8]*10])
    verticalN = len(verticalSpacing)
    sNx = 28
    sNy = 44
    nSx = 1
    nSy = 1
    nPx = 24
    nPy = 6
    RiverIndices = {0: ('Iwest', 17, 19),
                    1: ('Jsouth', 17, 19),
                    2: ('Ieast', 595, 21),
                    3: ('Jsouth', 595, 21),
                    4: ('Ieast', 595, 20)}
    smagorinskyCoeff = 0.3    # Smagorinsky coefficient for horizontal viscosity
    HorizDiffusivity = Kh(100)
    HorizViscosity = horizDiffViscosity(100, deltaT)

@dataclass
class options50:
    deltaT = 4
    minX = 672*2
    minY = 264*2
    betaVS = 0.5
    alphaVS = 0.0303
    verticalN = 100
    verticalSpacing = betaVS * np.exp(alphaVS * np.arange(0, verticalN))
    sNx = 28
    sNy = 44
    nSx = 1
    nSy = 1
    nPx = 24*2
    nPy = 6*2
    smagorinskyCoeff = 0.3    # Smagorinsky coefficient for horizontal viscosity
    HorizDiffusivity = Kh(50)
    HorizViscosity = horizDiffViscosity(50, deltaT)
    RiverIndices = {}

# List of explicitly supported discretizations
Discretization = {
    1000 : options1000,
    450 : options450,
    200 : options200,
    100 : options100,
    50 : options50
}

@dataclass
class HydrodynamicParameters:
    salinityValue = 0.05
    adamsBashforthStabilizingTerm = 0.03
    outputFrequency = 3600    # Every hour of simulation time
    monitorFrequency = 3600   # Every hour of simulation time
    albedo_value = 0.066      # Surface albedo value
    vertDiffusivity = 1.4e-7  # Vertical diffusivity
    vertViscosity = 1e-6      # Viscosity
    bottomDrag = 0.0025       # Quadratic bottom drag coefficient
    DaltonCoeff = 0.053       # Dalton coefficient for surface energy transfer

    # SHL2 temperature profile for Jan 15, 2019, to be used in case no initial pickup
    # file is given.
    temperatureSimstrat = ([
        7.54, 7.5442, 7.5483, 7.5321, 7.5022, 7.4724, 7.449, 7.4449, 7.4409, 7.44, 7.44,
        7.4399, 7.4377, 7.4356, 7.4334, 7.4312, 7.43, 7.43, 7.43, 7.43, 7.43, 7.43, 7.43,
        7.43, 7.43, 7.43, 7.43, 7.43, 7.43, 7.43, 7.43, 7.43, 7.43, 7.43, 7.43, 7.4299,
        7.4291, 7.4284, 7.4277, 7.427, 7.4263, 7.4256, 7.4249, 7.4241, 7.4234, 7.4227,
        7.422, 7.4213, 7.4206, 7.4156, 7.3933, 7.3711, 7.3488, 7.3266, 7.3043, 7.2821,
        7.2598, 7.2376, 7.2153, 7.1931, 7.1709, 7.1486, 7.1264, 7.1041, 7.0819, 7.0596,
        7.0374, 7.0151, 6.9929, 6.9707, 6.9484, 6.9262, 6.9039, 6.8817, 6.8594, 6.8372,
        6.8149, 6.7927, 6.7704, 6.7482, 6.726, 6.7037, 6.6815, 6.6592, 6.637, 6.6147,
        6.5925, 6.5702, 6.548, 6.5258, 6.5035, 6.4813, 6.459, 6.4368, 6.4145, 6.3923,
        6.37, 6.3478, 6.3289, 6.3233, 6.3177, 6.3121, 6.3066, 6.301, 6.2954, 6.2898,
        6.2842, 6.2787, 6.2731, 6.2675, 6.2619, 6.2564, 6.2508, 6.2452, 6.2396, 6.234,
        6.2285, 6.2229, 6.2173, 6.2117, 6.2061, 6.2006, 6.195, 6.1894, 6.1838, 6.1782,
        6.1727, 6.1671, 6.1615, 6.1559, 6.1503, 6.1448, 6.1392, 6.1336, 6.128, 6.1224,
        6.1169, 6.1113, 6.1057, 6.1001, 6.0946, 6.089, 6.0834, 6.0778, 6.0722, 6.0667,
        6.0611, 6.0555, 6.0499, 6.0443, 6.0388, 6.0332, 6.0292, 6.0274, 6.0255, 6.0237,
        6.0219, 6.02, 6.0182, 6.0164, 6.0145, 6.0127, 6.0108, 6.009, 6.0072, 6.0053,
        6.0035, 6.0017, 5.9998, 5.998, 5.9961, 5.9943, 5.9925, 5.9906, 5.9888, 5.987,
        5.9851, 5.9833, 5.9815, 5.9796, 5.9778, 5.9759, 5.9741, 5.9723, 5.9704, 5.9686,
        5.9668, 5.9649, 5.9631, 5.9613, 5.9594, 5.9576, 5.9557, 5.9539, 5.9521, 5.9502,
        5.9484, 5.9466, 5.9447, 5.9429, 5.941, 5.9393, 5.9377, 5.936, 5.9344, 5.9328,
        5.9311, 5.9295, 5.9279, 5.9262, 5.9246, 5.923, 5.9213, 5.9197, 5.9181, 5.9164,
        5.9148, 5.9132, 5.9115, 5.9099, 5.9083, 5.9066, 5.905, 5.9034, 5.9017, 5.9001,
        5.8985, 5.8968, 5.8952, 5.8936, 5.892, 5.8903, 5.8887, 5.8871, 5.8854, 5.8838,
        5.8822, 5.8805, 5.8789, 5.8773, 5.8756, 5.874, 5.8724, 5.8707, 5.8691, 5.8675,
        5.8658, 5.8642, 5.8626, 5.8609, 5.8595, 5.8585, 5.8574, 5.8563, 5.8553, 5.8542,
        5.8531, 5.8521, 5.851, 5.8499, 5.8488, 5.8478, 5.8467, 5.8456, 5.8446, 5.8435,
        5.8424, 5.8414, 5.8403, 5.8392, 5.8381, 5.8371, 5.836, 5.8349, 5.8339, 5.8328,
        5.8317, 5.8307, 5.8297, 5.829, 5.8283, 5.8276, 5.8269, 5.8262, 5.8254, 5.8247,
        5.824, 5.8233, 5.8226, 5.8219, 5.8212, 5.8204, 5.8196, 5.8185, 5.8174, 5.8162,
        5.8151, 5.814, 5.8129, 5.8118, 5.8107, 5.81, 5.81, 5.81, 5.8109, 5.8128, 5.8148,
        5.8168, 5.8188, 5.82, 5.82])
