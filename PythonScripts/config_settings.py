'''
Settings to initialize the parameters that are necessary to compile the
simulation code and interpolate the input data. By default, we simulate day
by day (stopping and continuing at midnight).
  startTime   - global start of the simulation
  endTime     - target end of the simulation
  meshSize    - horizontal mesh size for the grid
  withMPI     - whether to compile the hydrodynamic model with MPI
  withRivers  - whether to use river measurements from BAFU. This option exists
                because it is difficult to generate a proper configuration file
                for each mesh size. This option includes both river flow and
                water level measurements
  initialCond - In this case, we need to supply an initial pickup file for the
                first iteration
'''

from datetime import datetime, timezone
from dataclasses import dataclass
import socket

startTime   = datetime(year = 2020, month = 6, day = 3, tzinfo = timezone.utc)
endTime     = datetime(year = 2020, month = 6, day = 10, tzinfo = timezone.utc)

# Physical model
meshSize    = 200
withRivers  = True
initialCond = False

# Parallelization and other options
withMPI     = True

# Paths to the some of the input files, depending on what system we are.
machine = socket.gethostname()

@dataclass
class FilePaths:
    if machine == 'ArturPC':
        DatasetsPath = '/home/artur/Desktop/HydrodynamicData'
        ScratchPath = '/home/artur/Desktop/NoBackup/interpMS/'
        MeteoSwissDataPath = '/home/artur/Desktop/Inputs/MeteoSwiss'
    elif 'siam-linux' in machine:
        DatasetsPath = '/local/safinart/Input'
        ScratchPath = '/local/safinart/interpMS/'
        MeteoSwissDataPath = '/local/safinart/MeteoSwiss/'
    elif 'daint' in machine:
        DatasetsPath = '/store/sdsc/sd06/HydrodynamicData'
        ScratchPath = '/scratch/snx3000/asafin/interpMS/'
        MeteoSwissDataPath = '/store/sdsc/sd06/Observ_data/MeteoSwiss/'
    elif 'jupyter' in machine:                      # Renku instance
        DatasetsPath = '/work/datalakescopy/hydrodynamicdata'
        ScratchPath = '/work/datalakescopy/interpMS'
        LSTMpath = ''
        MeteoSwissDataPath = '/work/datalakescopy/MeteoSwiss/'
    else:
        raise ValueError('Option not found for class Filepaths')

    GridRasterFile = f'{DatasetsPath}/grid.ascii'
    BafuDataPath = f'{DatasetsPath}/BAFU/'
    LeXploreDataPath = f'{DatasetsPath}/LeXplore/'
